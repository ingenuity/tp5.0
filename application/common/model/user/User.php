<?php
//公共模块，数据信息的修改

namespace app\common\model\User;

use think\Model;

class User extends Model
{
    // 设置数据表 =>用户表
    protected $table = 'pre_user';

    //自动写入时间
    protected $autoWriteTimestamp = true;
    //创建时间 => 注册时间
    protected $createTime = 'createtime';
    //更新时间
    protected $updateTime = false;

    // 追加字段
    protected $appnd = [
        'sex_text',
        'avatar_cdn',
        'cover_cdn',
		'region_text'
    ];

    //设置不存在的字段
    public function getSexTextAttr($value, $data)
    {
        $SexList = [0 => '保密', 1 => '男', 2 => '女'];

        return $SexList[$data['sex']];
    }

    //头像
    public function getAvatarCdnAttr($value,$data)
    {
        $avatar = @is_file('.'.$data['avatar']) ? $data['avatar'] : '/static/home/res/images/avatar/default.png';
        //halt($data);
        // $cdn = config('cdn');
        $cdn = model('Config')->where(['key' => 'SiteUrl'])->value('value');
        return $cdn.$avatar;
    }
    //背景图片
    public function getCoverCdnAttr($value,$data)
    {
        $cover = @is_file('.'.$data['cover']) ? $data['cover'] : '/static/home/res/images/avatar/common.png';
        // halt($data);
        $cdn = config('cdn');
        return $cdn.$cover;
    }
    //地区字段
    public function getRegionTextAttr($value,$data)
    {
        $ReogionList = [];
        if(!empty($data['province']))
        {
            $ReogionList[] = model('Region')->where(['code' => $data['province']])->value('name');
        }
        if(!empty($data['city']))
        {
            $ReogionList[] = model('Region')->where(['code' => $data['city']])->value('name');
        }
        if(!empty($data['district']))
        {
            $ReogionList[] = model('Region')->where(['code' => $data['district']])->value('name');
        }
        return implode('-',$ReogionList);
    }
    //关联查询
    public function provinces()
    {
        /**
         *  belongsTo 相对
         * 第一个参数 模型名
         * 第二个参数 关联外键
         * 第三个参数 关联主键
         * 第四个参数 模型别名
         * 第五个参数 链表方法（JOIN）
         */
        return $this->belongsTo('app\common\model\Region','province','code',[],'LEFT')->setEagerlyType(0);
    }
    public function citys()
    {
        /**
         *  belongsTo 相对
         * 第一个参数 模型名
         * 第二个参数 关联外键
         * 第三个参数 关联主键
         * 第四个参数 模型别名
         * 第五个参数 链表方法（JOIN）
         */
        return $this->belongsTo('app\common\model\Region','city','code',[],'LEFT')->setEagerlyType(0);
    }
    public function districts()
    {
        /**
         *  belongsTo 相对
         * 第一个参数 模型名
         * 第二个参数 关联外键
         * 第三个参数 关联主键
         * 第四个参数 模型别名
         * 第五个参数 链表方法（JOIN）
         */
        return $this->belongsTo('app\common\model\Region','district','code',[],'LEFT')->setEagerlyType(0);
    }
}
