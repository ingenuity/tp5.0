<?php

namespace app\common\model\product;

use think\Model;

class Vip extends Model
{
    protected $table = 'pre_product_vip';

    public function product()
    {
        return $this->belongsTo('app\common\model\Product\Product','proid','id',[],'RIGHT')->setEagerlyType(0);
    }
}
