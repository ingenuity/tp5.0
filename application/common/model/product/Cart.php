<?php

namespace app\common\model\product;

use think\Model;

class Cart extends Model
{
    // 购物车
    protected $table = 'pre_cart';

    public function product()
    {
        return $this->belongsTo('app\common\model\Product\Product','proid','id',[],'INNER')->setEagerlyType(0);
    }
}
