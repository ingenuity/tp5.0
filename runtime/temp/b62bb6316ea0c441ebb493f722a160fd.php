<?php if (!defined('THINK_PATH')) exit(); /*a:7:{s:71:"F:\!Abeifen\WWW\tp5.0\public/../application/home\view\user\profile.html";i:1654155958;s:55:"F:\!Abeifen\WWW\tp5.0\application\home\view\layout.html";i:1653476814;s:60:"F:\!Abeifen\WWW\tp5.0\application\home\view\common\meta.html";i:1653471571;s:62:"F:\!Abeifen\WWW\tp5.0\application\home\view\common\script.html";i:1652864157;s:62:"F:\!Abeifen\WWW\tp5.0\application\home\view\common\header.html";i:1654352686;s:60:"F:\!Abeifen\WWW\tp5.0\application\home\view\common\menu.html";i:1653471554;s:62:"F:\!Abeifen\WWW\tp5.0\application\home\view\common\footer.html";i:1652755560;}*/ ?>
<!-- 主页面 -->
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- 引用样式 -->
    <!--  -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>创领论坛</title>
<link rel="stylesheet" href="/static/home/res/layui/css/layui.css">
<link rel="stylesheet" href="/static/home/res/css/global.css">
<link rel="stylesheet" href="/static/home/res/css/common.css">
    <!-- 引用js  -->
    <script src="/static/home/res/layui/layui.js"></script>
<script>
layui.cache.page = '';
layui.cache.user = {
  username: '游客'
  ,uid: -1
  ,avatar: '/static/home/res/images/avatar/00.jpg'
  ,experience: 83
  ,sex: '男'
};
layui.config({
  version: "3.0.0"
  ,base: '/static/home/res/mods/' //这里实际使用时，建议改成绝对路径
}).extend({
  fly: 'index'
}).use('fly');
</script>

</head>
<body>
    <!-- 引用头部 -->
    <!-- 头部公共样式 -->
<div class="fly-header layui-bg-black">
    <div class="layui-container">
        <a class="fly-logo" href="<?php echo url('home/index/index'); ?>">
            <img src="/static/home/res/images/logo.png" alt="layui" />
        </a>

        <ul class="layui-nav fly-nav-user">
            <!-- 未登入的状态 -->
            <!-- <li class="layui-nav-item">
          <a class="iconfont icon-touxiang layui-hide-xs" href="user/login.html"></a>
        </li>
        <li class="layui-nav-item">
          <a href="user/login.html">登入</a>
        </li>
        <li class="layui-nav-item">
          <a href="user/reg.html">注册</a>
        </li>
        <li class="layui-nav-item layui-hide-xs">
          <a href="/app/qq/" onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})" title="QQ登入" class="iconfont icon-qq"></a>
        </li>
        <li class="layui-nav-item layui-hide-xs">
          <a href="/app/weibo/" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})" title="微博登入" class="iconfont icon-weibo"></a>
        </li> -->

            <!-- 登入后的状态 -->
            <?php if(\think\Request::instance()->action() != 'login' && \think\Request::instance()->action() !='register'): if($LoginUser): ?>
            <li class="layui-nav-item">
              <a href="<?php echo url('admin/index/index'); ?>">后台</a>
            </li>
            <li class="layui-nav-item">
                <a class="fly-nav-avatar" href="javascript:;">
                    <cite class="layui-hide-xs"><?php echo $LoginUser['nickname']; ?></cite>
                    <i class="iconfont icon-renzheng layui-hide-xs" title="认证信息：<?php echo $LoginUser['nickname']; ?>"></i>
                    <i class="layui-badge fly-badge-vip layui-hide-xs">VIP <?php echo $LoginUser['vip']; ?></i>
                    <img src="<?php echo $LoginUser['avatar_cdn']; ?>" alt="<?php echo $LoginUser['nickname']; ?>">
                </a>
                <dl class="layui-nav-child">
                    <dd>
                        <a href="<?php echo url('home/user/profile'); ?>"><i class="layui-icon">&#xe620;</i>基本资料</a>
                    </dd>
                    <dd>
                        <a href="<?php echo url('home/user/message'); ?>"><i class="iconfont icon-tongzhi" style="top: 4px"></i>我的消息</a>
                    </dd>
                    <dd>
                        <a href="<?php echo url('home/user/index'); ?>"><i class="layui-icon"
                                style="margin-left: 2px; font-size: 22px">&#xe68e;</i>我的主页</a>
                    </dd>
                    <hr style="margin: 5px 0" />
                    <dd><a href="<?php echo url('home/index/logout'); ?>" style="text-align: center">退出</a></dd>
                </dl>
            </li>
            <?php else: ?>
            <!-- 未登入的状态 -->
            <li class="layui-nav-item">
                <a class="iconfont icon-touxiang layui-hide-xs" href="<?php echo url('home/user/login'); ?>"></a>
            </li>
            <li class="layui-nav-item">
              <a href="<?php echo url('admin/index/index'); ?>">后台</a>
            </li>
            <li class="layui-nav-item">
                <a href="<?php echo url('home/index/login'); ?>">登入</a>
            </li>
            <li class="layui-nav-item">
                <a href="<?php echo url('home/index/register'); ?>">注册</a>
            </li>
            <!-- <li class="layui-nav-item layui-hide-xs">
        <a
          href="/app/qq/"
          onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})"
          title="QQ登入"
          class="iconfont icon-qq"
        ></a>
      </li>
      <li class="layui-nav-item layui-hide-xs">
        <a
          href="/app/weibo/"
          onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})"
          title="微博登入"
          class="iconfont icon-weibo">
        </a>
      </li> -->
            <?php endif; endif; ?>
        </ul>
    </div>
</div>
    
    <!-- 资料修改 -->
<style>
  .layui-form-item .layui-input-inline{
      width: 160px;
  }
</style>

<div class="layui-container fly-marginTop fly-user-main">
  
  <!-- 主页右上角下拉框样式 -->
<ul class="layui-nav layui-nav-tree layui-inline" lay-filter="user">
    <li class="layui-nav-item">
      <a href="<?php echo url('home/user/index'); ?>">
        <i class="layui-icon">&#xe609;</i>
        我的主页
      </a>
    </li>
    <li class="layui-nav-item <?php echo $navTitle=='用户中心'?'layui-this' : ''; ?>">
      <a href="<?php echo url('home/user/home'); ?>">
        <i class="layui-icon">&#xe612;</i>
        用户中心
      </a>
    </li>
    <li class="layui-nav-item <?php echo $navTitle=='基本资料'?'layui-this' : ''; ?>">
      <a href="<?php echo url('home/user/profile'); ?>">
        <i class="layui-icon">&#xe620;</i>
        基本资料
      </a>
    </li>
    <li class="layui-nav-item <?php echo $navTitle=='消息列表'?'layui-this' : ''; ?>">
      <a href="<?php echo url('home/user/message'); ?>">
        <i class="layui-icon">&#xe620;</i>
        消息列表
      </a>
    </li>
    <li class="layui-nav-item <?php echo $navTitle=='积分消费中心'?'layui-this' : ''; ?>">
      <a href="<?php echo url('home/user/record'); ?>">
        <i class="layui-icon">&#xe611;</i>
        积分消费记录
      </a>
    </li>
  </ul>

  <div class="site-tree-mobile layui-hide">
    <i class="layui-icon">&#xe602;</i>
  </div>
  <div class="site-mobile-shade"></div>
  
  <div class="site-tree-mobile layui-hide">
    <i class="layui-icon">&#xe602;</i>
  </div>
  <div class="site-mobile-shade"></div>

  <div class="fly-panel fly-panel-user" pad20>
      <div class="layui-tab layui-tab-brief" lay-filter="user">
          <ul class="layui-tab-title" id="LAY_mine">
              <li class="layui-this" lay-id="info">我的资料</li>
              <li lay-id="avatar">头像</li>
              <li lay-id="pass">密码</li>
              <li lay-id="cover">封面图</li>
          </ul>
          <div class="layui-tab-content" style="padding: 20px 0;">
              <div class="layui-form layui-form-pane layui-tab-item layui-show">
                  <form method="post">
                      <input type="hidden" name="action" value="profile">

                      <!-- 邮箱 -->
                      <div class="layui-form-item">
                          <label for="L_email" class="layui-form-label">邮箱</label>
                          <div class="layui-input-inline">
                              <input type="text" id="L_email" name="email" value="<?php echo $LoginUser['email']; ?>" disabled required lay-verify="email"
                                  autocomplete="off" value="" class="layui-input">
                          </div>
                          
                          <?php if($LoginUser['auth'] == 0): ?>
                          <div class="layui-form-mid layui-word-aux">该邮箱未认证，需
                              <a id="auth" style="font-size: 12px; color: #4f99cf;cursor: pointer;">验证邮箱</a>。
                          </div>
                          <?php endif; ?>
                      </div>

                      <!-- 昵称，性别 -->
                      <div class="layui-form-item">
                          <label for="L_username" class="layui-form-label">昵称</label>
                          <div class="layui-input-inline">
                              <input type="text" id="L_username" name="nickname" value="<?php echo $LoginUser['nickname']; ?>" required lay-verify="required"
                                  autocomplete="off" value="" class="layui-input">
                          </div>

                          <!-- 性别 -->
                          <div class="layui-inline">
                              <div class="layui-input-inline" style="width: 245px;">
                                  <input type="radio" name="sex" value="0" <?php echo $LoginUser['sex']==0?'checked' : ''; ?> title="保密">保密
                                  <input type="radio" name="sex" value="1" <?php echo $LoginUser['sex']==1?'checked' : ''; ?> title="男">男
                                  <input type="radio" name="sex" value="2" <?php echo $LoginUser['sex']==2?'checked' : ''; ?> title="女">女
                              </div>
                          </div>
                      </div>

                      <!-- 地址 -->
                      <div class="layui-form-item">
                          <!-- 省份 -->
                          <label for="province" class="layui-form-label">省份</label>
                          <div class="layui-input-inline">
                             <select name="province" id="province" lay-filter="province">
                                 <option value="0">请选择</option>
                                 <?php foreach($provinces as $item): ?>
                                  <option value="<?php echo $item['code']; ?>" <?php echo $item['code']==$LoginUser['province']?'selected' : ''; ?>><?php echo $item['name']; ?></option>
                                 <?php endforeach; ?>
                             </select>
                          </div>

                          <!-- 市 -->
                          <label for="city" class="layui-form-label">市</label>
                          <div class="layui-input-inline">
                             <select name="city" id="city" lay-filter="city">
                                 <option value="0">请选择</option>
                                 <?php foreach($citys as $item): ?>
                                  <option value="<?php echo $item['code']; ?>" <?php echo $item['code']==$LoginUser['city']?'selected' : ''; ?>><?php echo $item['name']; ?></option>
                                 <?php endforeach; ?>
                             </select>
                          </div>

                          <!-- 区 -->
                          <label for="district" class="layui-form-label">区</label>
                          <div class="layui-input-inline">
                             <select name="district" id="district">
                                 <option value="0">请选择</option>
                                 <?php foreach($districts as $item): ?>
                                  <option value="<?php echo $item['code']; ?>" <?php echo $item['code']==$LoginUser['district']?'selected' : ''; ?>><?php echo $item['name']; ?></option>
                                 <?php endforeach; ?>
                             </select>
                          </div>
                      </div>
                      
                      <!-- 签名 -->
                      <div class="layui-form-item layui-form-text">
                          <label for="L_sign" class="layui-form-label">签名</label>
                          <div class="layui-input-block">
                              <textarea placeholder="随便写些什么刷下存在感" id="L_sign" name="content" autocomplete="off"
                                  class="layui-textarea" style="height: 80px;"><?php echo $LoginUser['content']; ?></textarea>
                          </div>
                      </div>
                      <div class="layui-form-item">
                          <button class="layui-btn" key="set-mine" lay-submit>确认修改</button>
                      </div>
                  </form>
              </div>

              <!-- 头像 -->
              <div class="layui-form layui-form-pane layui-tab-item">
                  <form method="post" enctype="multipart/form-data">
                      <input type="hidden" name="action" value="avatar">
                      <div class="layui-form-item">
                          <div class="avatar-add">
                              <p>建议尺寸168*168，支持jpg、png、gif，最大不能超过50KB</p>
                              <button type="button" class="layui-btn upload-img" onclick="avatar.click()">
                                  <i class="layui-icon">&#xe67c;</i>选择头像
                              </button>
                              <input type="file" name="avatar" id="avatar" hidden>
                              <img src="<?php echo !empty($LoginUser['avatar'])?$LoginUser['avatar'] : '/static/home/res/images/upload.png'; ?>" id="img">
                              <span class="loading"></span>
                          </div>
                      </div>
                      <div class="layui-form-item">
                          <button class="layui-btn" key="set-mine" lay-submit>上传</button>
                      </div>
                  </form>
              </div>

              <!-- 密码 -->
              <div class="layui-form layui-form-pane layui-tab-item">
                  <form action="/user/repass" method="post">
                      <div class="layui-form-item">
                          <label for="L_nowpass" class="layui-form-label">当前密码</label>
                          <div class="layui-input-inline">
                              <input type="password" id="L_nowpass" name="nowpass" required lay-verify="required"
                                  autocomplete="off" class="layui-input">
                          </div>
                      </div>
                      <div class="layui-form-item">
                          <label for="L_pass" class="layui-form-label">新密码</label>
                          <div class="layui-input-inline">
                              <input type="password" id="L_pass" name="pass" required lay-verify="required"
                                  autocomplete="off" class="layui-input">
                          </div>
                          <div class="layui-form-mid layui-word-aux">6到16个字符</div>
                      </div>
                      <div class="layui-form-item">
                          <label for="L_repass" class="layui-form-label">确认密码</label>
                          <div class="layui-input-inline">
                              <input type="password" id="L_repass" name="repass" required lay-verify="required"
                                  autocomplete="off" class="layui-input">
                          </div>
                      </div>
                      <div class="layui-form-item">
                          <button class="layui-btn" key="set-mine" lay-filter="*" lay-submit>确认修改</button>
                      </div>
                  </form>
              </div>

              <!-- 背景图片 -->
              <div class="layui-form layui-form-pane layui-tab-item">
                <form method="post" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="cover">
                    <div class="layui-form-item">
                        <div class="cover-add">
                            <p>建议尺寸168*168，支持jpg、png、gif，最大不能超过50KB</p>
                            <button type="button" class="layui-btn upload-img" onclick="cover.click()">
                                <i class="layui-icon">&#xe67c;</i>选择图片
                            </button>
                            <input type="file" name="cover" id="cover" hidden>
                            <img src="<?php echo !empty($LoginUser['cover'])?$LoginUser['cover'] : '/static/home/res/images/upload.png'; ?>" id="coverimg">
                            <span class="loading"></span>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <button class="layui-btn" key="set-mine" lay-submit>上传</button>
                    </div>
                </form>
              </div>
          </div>

      </div>
  </div>
</div>
</div>
<script>
  layui.use(['layer','form'],function(){
      var $ = layui.jquery,
      layer = layui.layer,
      form = layui.form

      // 邮箱认证
      $('#auth').click(function(){
          let email = $('#L_email').val()
          
          // 发起异步请求
          $.ajax({
              type:'post',
              url:`<?php echo url('home/user/email'); ?>`,
              data:{
                  email
              },
              dataType:'json',
              success:function(res)
              {
                  if(res.code === 1)
                  {
                      layer.msg(res.msg)
                  }else{
                      layer.msg(res.msg)
                  }
              }
          })
      })

      form.on('select(province)', function(data){
          // console.log(data);
          let code = data.value
          
          $.ajax({
              type:'post',
              url:`<?php echo url('home/user/area'); ?>`,
              data:{
                  code
              },
              dataType:'json',
              success:function(res)
              {
                  if(res.code === 1)
                  {
                      let option = ''

                      res.data.forEach(item => {
                          option += `<option value="${item.code}">${item.name}</option>`
                      })

                      $('#city').html(option)

                      let code = res.data[0].code ? res.data[0].code : ''

                      GetArea(code,"#district")

                      form.render('select')
                  }
              }
          })
      })

      form.on("select(city)",function(data){
          let code = data.value

          GetArea(code,"#district")
      })

      function GetArea(code,elem)
      {
          $.ajax({
              type:'post',
              url:`<?php echo url('home/user/area'); ?>`,
              data:{
                  code
              },
              dataType:'json',
              success:function(res)
              {
                  if(res.code === 1)
                  {
                      let option = ''

                      res.data.forEach(item => {
                          option += `<option value="${item.code}">${item.name}</option>`
                      })

                      $(elem).html(option)

                      form.render('select')
                  }

                  
              }
          })

      }

      // 图片预览函数
      function GetObjectURL(file) {
          // createObjectURL  给一个文件对象 可以提取出一个url本地地址出来
          var url = null;

          //判断是否为IE浏览器还是google还是其他浏览器
          if (window.createObjectURL != undefined) {
          url = window.createObjectURL(file)
          } else if (window.URL != undefined) {
          url = window.URL.createObjectURL(file)
          } else if (window.webkitURL != undefined) {
          url = window.webkitURL.createObjectURL(file)
          }

          return url
      }

      $('#avatar').change(function(){
          let file = $(this)[0].files[0] ? $(this)[0].files[0] : {}
          let url = GetObjectURL(file)

          $('#img').attr('src',url)

      })
      $('#cover').change(function(){
          let file = $(this)[0].files[0] ? $(this)[0].files[0] : {}
          let url = GetObjectURL(file)

          $('#coverimg').attr('src',url)

      })

  })
</script>
    

    <!-- 引用底部 -->
    <div class="fly-footer">
    <p>2017 &copy; <a href="http://www.layui.com/" target="_blank">Fly技术社区</a></p>
</div>
</body>
</html>