<?php

namespace app\api\controller\Product;

use think\Controller;

class Cart extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->CartModel = model('Product.Cart');
    }

    public function index()
    {
        $userid = $this->request->param('userid',0);

        $CartList = $this->CartModel->with('product')->where(['userid' => $userid])->select();

        if(!$CartList)
        {
            $this->error('购物车空空，请去添加商品');
        }

        $data = [
            'CartList' => $CartList
        ];

        $this->success('查询成功',null,$data);


    }

    public function nums()
    {
        $nums = $this->request->param('nums',0);

        $id = $this->request->param('id',0);
        // halt($id);
        $cart = $this->CartModel->find($id);

        if(!$cart)
        {
            $this->error('该商品未加入购物车里');
        }

        $data = [
            'id' => $id,
            'nums' => $nums,
            'total' => bcmul($cart['price'],$nums)
        ];

        $result = $this->CartModel->isUpdate(true)->save($data);

        if($result === FALSE)
        {
            $this->error('更新该商品数量失败');
        }else{
            $this->success('更新成功');
        }

    }

    public function add()
    {
        $userid = $this->request->param('userid');
        $proid = $this->request->param('proid');
        $num = $this->request->param('num');

        $product = model('Product.Product')->find($proid);

        // 看一下库存是否足够
        if($product['stock'] <= 0)
        {
            $this->error('该商品库存不足');
        }

        $data = [
            'userid' => $userid,
            'proid' => $proid,
            'nums' => $num,
            'price' => $product['vip_price'] > 0 ? $product['vip_price'] : $product['price'],
            'total' => $product['vip_price'] > 0 ? $product['vip_price'] : $product['price']
        ];

        // 查询购物车是否存在该商品
        $Cart = model('Product.Cart')->where(['userid' => $userid,'proid' => $proid])->find();

        if(!empty($Cart))
        {
            // 能查询购物车的确存在该商品 => 更新数据

            // 商品数量
            $nums = bcadd($Cart['nums'],$num);

            $UpdateData = [
                'id' => $Cart['id'],
                'nums' => $nums,
                'total' => bcmul($data['price'],$nums)
            ];

            $result = $this->CartModel->isUpdate(true)->save($UpdateData);
        }else{
            // 查询到购物车没有该商品 => 添加
            $result = $this->CartModel->validate('common/Product/Cart')->save($data);
        }

        if($result === FALSE)
        {
            $this->error($this->CartModel->getError());
        }else{
            $this->success('添加购物车成功');
        }
        
    }

    public function list()
    {
        $cartids = $this->request->param('ids');

        $list = $this->CartModel->with(['product'])->whereIn('cart.id',$cartids)->select();

        if(!$list)
        {
            $this->error('所选的商品不存在，请重新选择');
        }

        $this->success('查询成功',null,$list);
    }
}
