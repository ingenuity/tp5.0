<?php

namespace app\common\validate\Post;

//引入底层的验证器类
use think\Validate;

/**
 * 帖子验证器
 */
class Post extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'title' => 'require',
        'content' => 'require',
        'point' => 'require|number|egt:0',  //给字段设置 >= 0
        'state' => 'number|in:0,1,2,3',  //给字段设置范围
        'userid' => 'require', //必填
        'cateid' => 'require', //必填
    ];

    /**
     * 提示消息
     */
    protected $message = [
        'title.require' => '帖子标题必填',
        'content.require' => '帖子内容必填',
        'point.number'    => '积分必须是个数字',
        'point.egt'    => '积分值必须是大于等于0',
        'state.number'    => '状态必须是个数值',
        'userid.require'  => '用户身份未知',
        'cateid.require'  => '请选择对应的分类',
    ];
    
    /**
     * 验证场景
     */
    protected $scene = [
        'edit' => ['title','content','cateid']
    ];
}