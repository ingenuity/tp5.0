<?php

namespace app\common\model\User;

use think\Model;

class record extends Model
{
    // 用户消费记录
    protected $table = 'pre_user_record';
    // 自动写入时间
    protected $autoWriteTimestamp = true;
     // 创建时间 => 注册时间
     protected $createTime = 'createtime';
     // 更新时间
     protected $updateTime = false;
}
