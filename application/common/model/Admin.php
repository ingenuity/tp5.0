<?php

namespace app\common\model;

use think\Model;

class Admin extends Model
{
    // 管理员表
    protected $table ='pre_admin';

     // 追加字段
     protected $appnd = [
        'state_text',
    ];

    //设置不存在的字段
    public function getStateTextAttr($value, $data)
    {
        $StateList = [0 => '禁用', 1 => '正常'];

        return $StateList[$data['state']];
    }
}
