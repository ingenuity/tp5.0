<?php

namespace app\api\controller\User;

use PHPMailer\PHPMailer\PHPMailer;
use think\Controller;
use think\Request;

class Base extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->UserModel = model('User.User');
    }

    // 注册
    public function register()
    {
        $email = $this->request->param('email','');

        $password = $this->request->param('password','');

        // 注册时赠送的积分 商城的数据库 => 论坛的数据库
        $RegisterPoint = model('Config')->where('key','RegisterPoint')->value('value');

        // 生成密码盐
        $salt = build_ranstr();

        // 加密
        $password = md5($password.$salt);

        // 封装数据
        $data = [
            'email' => $email,
            'password' => $password,
            'salt' => $salt,
            'point' => $RegisterPoint,
            'sex' => 0,
            'auth' => 0,
            'vip' => 1,
        ];

        // 插入数据库
        $result = $this->UserModel->validate('common/User/User.vue')->save($data);

        if($result === FALSE)
        {
            $this->error($this->UserModel->getError());
        }else{
            $this->success('注册成功','/user/base/login');
        }
    }

    // 登录 
    public function login()
    {
        $params = $this->request->param();

        $User = $this->UserModel->where(['email' => $params['email']])->find();

        if(!$User)
        {
            $this->error('该用户不存在');
        }

        // 密码
        $password = md5($params['password'].$User['salt']);

        if($password != $User['password'])
        {
            $this->error('密码错误');
        }

        $data = [
            'id' => $User['id'],
            'email' => $User['email'],
            'nickname' => $User['nickname'],
            'avatar' => $User['avatar_cdn'],
            'cover' => $User['cover_cdn'],
            'sex' => $User['sex'],
            'sex_text' => $User['sex_text'],
            'province' => $User['province'],
            'city' => $User['city'],
            'district' => $User['district'],
            'createtime' => $User['createtime'],
            'content' => $User['content'],
            'point' => $User['point'],
            'vip' => $User['vip'],
            'auth' => $User['auth'],
            'region_text' => $User['region_text']
        ];

        $this->success('登录成功','/user/base/index',$data);
    }

    // 认证功能
    public function check()
    {
        $userid = $this->request->param('userid');

        $User = $this->UserModel->find($userid);

        if(!$User)
        {
            $this->error('非法登录');
        }

        $data = [
            'id' => $User['id'],
            'email' => $User['email'],
            'nickname' => $User['nickname'],
            'avatar' => $User['avatar_cdn'],
            'cover' => $User['cover_cdn'],
            'sex' => $User['sex'],
            'sex_text' => $User['sex_text'],
            'province' => $User['province'],
            'city' => $User['city'],
            'district' => $User['district'],
            'createtime' => $User['createtime'],
            'content' => $User['content'],
            'point' => $User['point'],
            'vip' => $User['vip'],
            'auth' => $User['auth'],
            'region_text' => $User['region_text']
        ];

        $this->success('认证成功',null,$data);
    }

    // 修改信息
    public function profile()
    {
        $params = $this->request->param();

        $user = $this->UserModel->find($params['id']);

        if(!$user)
        {
            $this->error('用户不存在');
        }

        // 封装数据
        $data = [
            'id' => $params['id'],
            'nickname' => $params['nickname'],
            'sex' => $params['sex'],
            'content' => $params['content']
        ];

        if(!empty($params['code']))
        {
            $parentpath = model('Region')->where(['code' => $params['code']])->value('parentpath');

            if(!$parentpath)
            {
                $this->error('所选地区不存在');
            }

            [$province,$city,$district] = explode(',',$parentpath);

            if($province)
            {
                $data['province'] = $province;
            }

            if($city)
            {
                $data['city'] = $city;
            }

            if($district)
            {
                $data['district'] = $district;
            }
        }

        if(!empty($params['password']))
        {
            // 密码盐
            $salt = build_ranstr();

            // 加密
            $data['password'] = md5($params['password'].$salt);

            $data['salt'] = $salt;
        }

        if(!empty($_FILES['avatar']) && $_FILES['avatar']['size'] > 0)
        {
            $avatar = upload('avatar');

            if($avatar['code'] == 0)
            {
                $this->error($avatar['msg']);
            }

            $data['avatar'] = $avatar['data'];
        }

        if(!empty($_FILES['cover']) && $_FILES['cover']['size'] > 0)
        {
            $cover = upload('cover');

            if($cover['code'] == 0)
            {
                $this->error($cover['msg']);
            }

            $data['cover'] = $cover['data'];
        }

        $result = $this->UserModel->isUpdate(true)->save($data);

        if($result === FALSE)
        {
            if(!empty($data['cover']) && $_FILES['cover']['size'] > 0)
            {
                @is_file('.' . $data['cover']) && unlink('.' . $data['cover']);
            }

            if(!empty($data['avatar']) && $_FILES['avatar']['size'] > 0)
            {
                @is_file('.' . $data['avatar']) && unlink('.' . $data['avatar']);
            }

            $this->error('修改失败');

        }else{
            if(!empty($data['cover']) && $_FILES['cover']['size'] > 0)
            {
                @is_file('.' . $user['cover']) && unlink('.' . $user['cover']);
            }

            if(!empty($data['avatar']) && $_FILES['avatar']['size'] > 0)
            {
                @is_file('.' . $user['avatar']) && unlink('.' . $user['avatar']);
            }

            $User = $this->UserModel->find($params['id']);

            $UserData = [
                'id' => $User['id'],
                'email' => $User['email'],
                'nickname' => $User['nickname'],
                'avatar' => $User['avatar_cdn'],
                'cover' => $User['cover_cdn'],
                'sex' => $User['sex'],
                'sex_text' => $User['sex_text'],
                'province' => $User['province'],
                'city' => $User['city'],
                'district' => $User['district'],
                'createtime' => $User['createtime'],
                'content' => $User['content'],
                'point' => $User['point'],
                'vip' => $User['vip'],
                'auth' => $User['auth'],
                'region_text' => $User['region_text']
            ];

            $this->success('修改成功','/user/base/index',$UserData);
        }

    }

    // 邮箱认证
    public function code()
    {
        $userid = $this->request->param('userid');

        $email = $this->request->param('email');

        $User = $this->UserModel->find($userid);

        if(!$User)
        {
            $this->error('该用户不存在');
        }
        if($email != $User['email'])
        {
            $this->error('该邮箱不存在');
        }

        $code = build_ranstr(4);

        session($User['id'],$code);

        $content = "验证码: $code";

        $mail = new PHPMailer(true);

        // 开启SMTP
        $mail->isSMTP(true);

        // SMTP服务器 网易邮箱 => smtp.163.com
        $mail->Host = "smtp.qq.com";

        // 开启认证
        $mail->SMTPAuth = true;

        // 发送内容支持HTML
        $mail->isHTML(true);

        // 账号
        $mail->Username = 'uytrewqqwerty@qq.com';

        // 密码 -> 不是邮箱密码，开启SMTP时授权码
        $mail->Password = 'qsonwqzducsnebhc';

        // 端口 服务器需要465端口
        $mail->Prot = 465;

        // 协议
        $mail->SMTPSecure = 'ssl';

        // 发送邮箱
        $mail->From = 'uytrewqqwerty@qq.com';

        // 发件人名字
        $mail->FromName = "创领论坛";

        // 接收回复的邮箱
        $mail->addReplyTo('uytrewqqwerty@qq.com',"创领论坛");

        // 收件人
        $mail->addAddress($User['email'],$User['nickname']);

        // 邮箱主题
        $mail->Subject = '账号认证';

        //设置多少个字符换行 80字符
        $mail->WordWrap = 80;

        // 发送HTML内容
        $mail->msgHTML($content);

        $result = $mail->send();

        if($result === FALSE)
        {
            $this->error('发送邮件失败');
        }else {
            $this->success('发送邮件成功');
        }

    }

    public function echeck()
    {
        $userid = $this->request->param('userid');

        $code = $this->request->param('code');

        $user = $this->UserModel->find($userid);

        if(!$user)
        {
            $this->error('该用户不存在');
        }

        $check = session($user['id']);

        if($code != $check)
        {
            $this->error('验证码错误');
        }

        $data = [
            'id' => $user['id'],
            'auth' => 1
        ];

        $result = $this->UserModel->isUpdate(true)->save($data);

        if($result === FALSE)
        {
            $this->error('验证失败');
        }else{
            $this->success('验证成功');
        }
    }
}
