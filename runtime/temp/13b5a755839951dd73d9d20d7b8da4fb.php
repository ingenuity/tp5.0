<?php if (!defined('THINK_PATH')) exit(); /*a:6:{s:74:"F:\!Abeifen\WWW\tp5.0\public/../application/admin\view\admin\userfile.html";i:1654415139;s:56:"F:\!Abeifen\WWW\tp5.0\application\admin\view\layout.html";i:1653877040;s:61:"F:\!Abeifen\WWW\tp5.0\application\admin\view\common\meta.html";i:1653968148;s:63:"F:\!Abeifen\WWW\tp5.0\application\admin\view\common\script.html";i:1653894646;s:61:"F:\!Abeifen\WWW\tp5.0\application\admin\view\common\menu.html";i:1654351541;s:63:"F:\!Abeifen\WWW\tp5.0\application\admin\view\common\header.html";i:1654353018;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="keywords" content="LightYear,LightYearAdmin,光年,后台模板,后台管理系统,光年HTML模板">
<meta name="description" content="Light Year Admin V4是一个基于Bootstrap v4.4.1的后台管理系统的HTML模板。">
<meta name="author" content="yinq">
<title><?php echo $system['SiteName']; ?>后台管理系统</title>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo $system['favicon']; ?>">
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<link rel="stylesheet" type="text/css" href="/static/admin/css/materialdesignicons.min.css">
<link rel="stylesheet" type="text/css" href="/static/admin/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/static/admin/css/animate.min.css">
<link rel="stylesheet" type="text/css" href="/static/admin/css/style.min.css">

    <script type="text/javascript" src="/static/admin/js/jquery.min.js"></script>
<script type="text/javascript" src="/static/admin/js/popper.min.js"></script>
<script type="text/javascript" src="/static/admin/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/static/admin/js/jquery.cookie.min.js"></script>
<script type="text/javascript" src="/static/admin/js/main.min.js"></script>
<script type="text/javascript" src="/static/admin/js/Chart.min.js"></script>
</head>
<body>
    <div class="lyear-layout-web">
        <div class="lyear-layout-container">

            <!-- 引用侧边菜单 -->
            <!--左侧导航-->
<aside class="lyear-layout-sidebar">

  <!-- logo -->
  <div id="logo" class="sidebar-header">
    <a href="<?php echo url('admin/index/index'); ?>"><img src="<?php echo $system['Logo']; ?>" title="LightYear" alt="LightYear" /></a>
  </div>
  <div class="lyear-layout-sidebar-info lyear-scroll">

    <nav class="sidebar-main">
      <ul class="nav-drawer">

        <!-- 后台首页 -->
        <li class="nav-item active"> <a href="<?php echo url('admin/index/index'); ?>"><i class="mdi mdi-home"></i>
            <span>后台首页</span></a> </li>

        <!-- 网络配置 -->
        <li class="nav-item"> <a href="<?php echo url('admin/config/index'); ?>"><i class="mdi mdi-tools"></i> <span>网站配置</span></a>
        </li>

        <!-- 信息管理 -->
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-account-multiple-outline"></i> <span>信息管理</span></a>
          <ul class="nav nav-subnav">
            <li> <a href="<?php echo url('admin/admin/user'); ?>"><i class="mdi mdi-account-alert-outline"></i>
                <span>用户信息管理</span></a> </li>
            <li> <a href="<?php echo url('admin/admin/admin'); ?>"><i class="mdi mdi-account-alert"></i> <span>管理员信息管理</span></a>
            </li>
          </ul>
        </li>

        <!-- 帖子管理 -->
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-account-multiple-outline"></i> <span>帖子管理</span></a>
          <ul class="nav nav-subnav">
            <li> <a href="<?php echo url('admin/post/index'); ?>"><i class="mdi mdi-account-alert-outline"></i><span>帖子信息管理</span></a> </li>
          </ul>
        </li>

        <!-- 元素配置 -->
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-palette"></i> <span>数据统计</span></a>
          <ul class="nav nav-subnav">
            <li> <a href="<?php echo url('admin/analysis/user'); ?>"><span>用户统计</span></a> </li>
          </ul>
        </li>

        <!-- 表单 -->
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-format-align-justify"></i> <span>表单</span></a>
          <ul class="nav nav-subnav">
            <li> <a href="lyear_forms_elements.html">基本元素</a> </li>
            <li> <a href="lyear_forms_input_group.html">输入框组</a> </li>
            <li> <a href="lyear_forms_radio.html">单选框</a> </li>
            <li> <a href="lyear_forms_checkbox.html">复选框</a> </li>
            <li> <a href="lyear_forms_switches.html">开关</a> </li>
            <li> <a href="lyear_forms_range.html">范围选择</a> </li>
          </ul>
        </li>

        <!-- 工具类 -->
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-tools"></i> <span>工具类</span></a>
          <ul class="nav nav-subnav">
            <li> <a href="lyear_utilities_borders.html">边框</a> </li>
            <li> <a href="lyear_utilities_colors.html">颜色</a> </li>
            <li> <a href="lyear_utilities_display.html">显示属性</a> </li>
            <li> <a href="lyear_utilities_flex.html">弹性布局</a> </li>
            <li> <a href="lyear_utilities_float.html">浮动</a> </li>
            <li> <a href="lyear_utilities_sizing.html">尺寸</a> </li>
            <li> <a href="lyear_utilities_spacing.html">间隔</a> </li>
            <li> <a href="lyear_utilities_stretched_link.html">延伸链接</a> </li>
            <li> <a href="lyear_utilities_text.html">文本</a> </li>
            <li> <a href="lyear_utilities_other.html">其他</a> </li>
          </ul>
        </li>

        <!-- 示例页面 -->
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-file-outline"></i> <span>示例页面</span></a>
          <ul class="nav nav-subnav">
            <li> <a href="lyear_pages_doc.html">文档列表</a> </li>
            <li> <a href="lyear_pages_gallery.html">图库列表</a> </li>
            <li> <a href="lyear_pages_config.html">网站配置</a> </li>
            <li> <a href="lyear_pages_rabc.html">设置权限</a> </li>
            <li> <a href="lyear_pages_add_doc.html">新增文档</a> </li>
            <li> <a href="lyear_pages_guide.html">表单向导</a> </li>
            <li> <a href="lyear_pages_data_table.html">表格插件</a> </li>
            <li> <a href="lyear_pages_login_1.html" target="_blank">登录页面1</a> </li>
            <li> <a href="lyear_pages_login_2.html" target="_blank">登录页面2</a> </li>
            <li> <a href="lyear_pages_login_3.html" target="_blank">登录页面3</a> </li>
            <li> <a href="lyear_pages_login_4.html" target="_blank">登录页面4</a> </li>
            <li> <a href="lyear_pages_error.html" target="_blank">错误页面</a> </li>
          </ul>
        </li>

        <!-- js插件 -->
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-language-javascript"></i> <span>JS 插件</span></a>
          <ul class="nav nav-subnav">
            <li> <a href="lyear_js_datepicker.html">日期选取器</a> </li>
            <li> <a href="lyear_js_colorpicker.html">选色器</a> </li>
            <li> <a href="lyear_js_chartjs.html">Chart.js</a> </li>
            <li> <a href="lyear_js_jconfirm.html">对话框</a> </li>
            <li> <a href="lyear_js_tags_input.html">标签插件</a> </li>
            <li> <a href="lyear_js_notify.html">通知消息</a> </li>
            <li> <a href="lyear_js_maxlength.html">长度判断</a> </li>
            <li> <a href="lyear_js_select.html">下拉选择</a> </li>
            <li> <a href="lyear_js_fullcalendar.html">日程插件</a> </li>
            <li> <a href="lyear_js_loading.html">loading插件</a> </li>
          </ul>
        </li>

        <!-- 多级菜单 -->
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-menu"></i> <span>多级菜单</span></a>
          <ul class="nav nav-subnav">
            <li> <a href="#!">一级菜单</a> </li>
            <li class="nav-item nav-item-has-subnav">
              <a href="#!">一级菜单</a>
              <ul class="nav nav-subnav">
                <li> <a href="#!">二级菜单</a> </li>
                <li class="nav-item nav-item-has-subnav">
                  <a href="#!">二级菜单</a>
                  <ul class="nav nav-subnav">
                    <li> <a href="#!">三级菜单</a> </li>
                    <li> <a href="#!">三级菜单</a> </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li> <a href="#!">一级菜单</a> </li>
          </ul>
        </li>
      </ul>
    </nav>

    <div class="sidebar-footer">
      <p class="copyright">Copyright &copy; 2019. <a target="_blank" href="http://lyear.itshubao.com">IT书包</a> All
        rights reserved.</p>
    </div>
  </div>

</aside>
<!--End 左侧导航-->

            <!-- 引用头部 -->
            <!--头部信息-->
<header class="lyear-layout-header">
      
    <nav class="navbar">
    
      <div class="navbar-left">
        <div class="lyear-aside-toggler">
          <span class="lyear-toggler-bar"></span>
          <span class="lyear-toggler-bar"></span>
          <span class="lyear-toggler-bar"></span>
        </div>
      </div>
      
      <ul class="navbar-right d-flex align-items-center">
        <li class="dropdown dropdown-notice">
          <span data-toggle="dropdown" class="icon-item">
            <i class="mdi mdi-bell-outline"></i>
            <span class="badge badge-danger">10</span>
          </span>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="lyear-notifications">
              
              <div class="lyear-notifications-title clearfix"  data-stopPropagation="true"><a href="#!" class="float-right">查看全部</a>你有 10 条未读消息</div>
              <div class="lyear-notifications-info lyear-scroll">
                <a href="#!" class="dropdown-item" title="树莓派销量猛增，疫情期间居家工作学习、医疗领域都需要它">树莓派销量猛增，疫情期间居家工作学习、医疗领域都需要它</a>
                <a href="#!" class="dropdown-item" title="GNOME 用户体验团队将为 GNOME Shell 提供更多改进">GNOME 用户体验团队将为 GNOME Shell 提供更多改进</a>
                <a href="#!" class="dropdown-item" title="Linux On iPhone 即将面世，支持 iOS 的双启动">Linux On iPhone 即将面世，支持 iOS 的双启动</a>
                <a href="#!" class="dropdown-item" title="GitHub 私有仓库完全免费面向团队提供">GitHub 私有仓库完全免费面向团队提供</a>
                <a href="#!" class="dropdown-item" title="Wasmtime 为 WebAssembly 增加 Go 语言绑定">Wasmtime 为 WebAssembly 增加 Go 语言绑定</a>
                <a href="#!" class="dropdown-item" title="红帽借“订阅”成开源一哥，首创者 Cormier 升任总裁">红帽借“订阅”成开源一哥，首创者 Cormier 升任总裁</a>
                <a href="#!" class="dropdown-item" title="Zend 宣布推出两项 PHP 新产品">Zend 宣布推出两项 PHP 新产品</a>
              </div>
              
            </div>
          </div>
        </li>
        <!--切换主题配色-->
        <li class="dropdown dropdown-skin">
          <span data-toggle="dropdown" class="icon-item"><i class="mdi mdi-palette"></i></span>
          <ul class="dropdown-menu dropdown-menu-right" data-stopPropagation="true">
            <li class="drop-title"><p>主题</p></li>
            <li class="drop-skin-li clearfix">
              <span class="inverse">
                <input type="radio" name="site_theme" value="default" id="site_theme_1" checked>
                <label for="site_theme_1"></label>
              </span>
              <span>
                <input type="radio" name="site_theme" value="dark" id="site_theme_2">
                <label for="site_theme_2"></label>
              </span>
              <span>
                <input type="radio" name="site_theme" value="translucent" id="site_theme_3">
                <label for="site_theme_3"></label>
              </span>
            </li>
            <li class="drop-title"><p>LOGO</p></li>
            <li class="drop-skin-li clearfix">
              <span class="inverse">
                <input type="radio" name="logo_bg" value="default" id="logo_bg_1" checked>
                <label for="logo_bg_1"></label>
              </span>
              <span>
                <input type="radio" name="logo_bg" value="color_2" id="logo_bg_2">
                <label for="logo_bg_2"></label>
              </span>
              <span>
                <input type="radio" name="logo_bg" value="color_3" id="logo_bg_3">
                <label for="logo_bg_3"></label>
              </span>
              <span>
                <input type="radio" name="logo_bg" value="color_4" id="logo_bg_4">
                <label for="logo_bg_4"></label>
              </span>
              <span>
                <input type="radio" name="logo_bg" value="color_5" id="logo_bg_5">
                <label for="logo_bg_5"></label>
              </span>
              <span>
                <input type="radio" name="logo_bg" value="color_6" id="logo_bg_6">
                <label for="logo_bg_6"></label>
              </span>
              <span>
                <input type="radio" name="logo_bg" value="color_7" id="logo_bg_7">
                <label for="logo_bg_7"></label>
              </span>
              <span>
                <input type="radio" name="logo_bg" value="color_8" id="logo_bg_8">
                <label for="logo_bg_8"></label>
              </span>
            </li>
            <li class="drop-title"><p>头部</p></li>
            <li class="drop-skin-li clearfix">
              <span class="inverse">
                <input type="radio" name="header_bg" value="default" id="header_bg_1" checked>
                <label for="header_bg_1"></label>                      
              </span>                                                    
              <span>                                                     
                <input type="radio" name="header_bg" value="color_2" id="header_bg_2">
                <label for="header_bg_2"></label>                      
              </span>                                                    
              <span>                                                     
                <input type="radio" name="header_bg" value="color_3" id="header_bg_3">
                <label for="header_bg_3"></label>
              </span>
              <span>
                <input type="radio" name="header_bg" value="color_4" id="header_bg_4">
                <label for="header_bg_4"></label>                      
              </span>                                                    
              <span>                                                     
                <input type="radio" name="header_bg" value="color_5" id="header_bg_5">
                <label for="header_bg_5"></label>                      
              </span>                                                    
              <span>                                                     
                <input type="radio" name="header_bg" value="color_6" id="header_bg_6">
                <label for="header_bg_6"></label>                      
              </span>                                                    
              <span>                                                     
                <input type="radio" name="header_bg" value="color_7" id="header_bg_7">
                <label for="header_bg_7"></label>
              </span>
              <span>
                <input type="radio" name="header_bg" value="color_8" id="header_bg_8">
                <label for="header_bg_8"></label>
              </span>
              </li>
            <li class="drop-title"><p>侧边栏</p></li>
            <li class="drop-skin-li clearfix">
              <span class="inverse">
                <input type="radio" name="sidebar_bg" value="default" id="sidebar_bg_1" checked>
                <label for="sidebar_bg_1"></label>
              </span>
              <span>
                <input type="radio" name="sidebar_bg" value="color_2" id="sidebar_bg_2">
                <label for="sidebar_bg_2"></label>
              </span>
              <span>
                <input type="radio" name="sidebar_bg" value="color_3" id="sidebar_bg_3">
                <label for="sidebar_bg_3"></label>
              </span>
              <span>
                <input type="radio" name="sidebar_bg" value="color_4" id="sidebar_bg_4">
                <label for="sidebar_bg_4"></label>
              </span>
              <span>
                <input type="radio" name="sidebar_bg" value="color_5" id="sidebar_bg_5">
                <label for="sidebar_bg_5"></label>
              </span>
              <span>
                <input type="radio" name="sidebar_bg" value="color_6" id="sidebar_bg_6">
                <label for="sidebar_bg_6"></label>
              </span>
              <span>
                <input type="radio" name="sidebar_bg" value="color_7" id="sidebar_bg_7">
                <label for="sidebar_bg_7"></label>
              </span>
              <span>
                <input type="radio" name="sidebar_bg" value="color_8" id="sidebar_bg_8">
                <label for="sidebar_bg_8"></label>
              </span>
            </li>
          </ul>
        </li>
        <!--切换主题配色-->
        <li class="dropdown dropdown-profile">
          <a href="javascript:void(0)" data-toggle="dropdown" class="dropdown-toggle">
            <!-- <img class="img-avatar img-avatar-48 m-r-10" src="images/users/avatar.jpg" alt="笔下光年" /> -->
            <span><?php echo $LoginAdmin['nickname']; ?></span>
          </a>
          <ul class="dropdown-menu dropdown-menu-right">
            <li>
              <a class="dropdown-item" href="lyear_pages_profile.html"><i class="mdi mdi-account"></i> 个人信息</a>
            </li>
            <li>
              <a class="dropdown-item" href="lyear_pages_edit_pwd.html"><i class="mdi mdi-lock-outline"></i> 修改密码</a>
            </li>
            <li>
              <a class="dropdown-item" href="javascript:void(0)"><i class="mdi mdi-delete"></i> 清空缓存</a>
            </li>
            <li class="dropdown-divider"></li>
            <li>
              <a class="dropdown-item" href="<?php echo url('admin/index/logout'); ?>"><i class="mdi mdi-logout-variant"></i> 退出登录</a>
            </li>
          </ul>
        </li>
      </ul>
      
    </nav>
    
  </header>
  <!--End 头部信息-->

            <!--修改用户信息-->

<main class="lyear-layout-content">

    <div class="container-fluid p-t-15">

        <div class="row">

            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs page-tabs pt-2 pl-3 pr-3">
                        <li class="nav-Userfile"> <a class="nav-link active" style="cursor: pointer;">修改管理员信息</a> </li>
                    </ul>
                    <div class="well pl-3 px-3">
                        <table class="table">

                            <form method="post">
                                <input type="hidden" name="action" value="profile">

                                <!-- 邮箱 -->
                                <thead>
                                    <th>
                                        <div class="layui-form-Userfile">
                                            <label for="L_email" class="layui-form-label">邮箱</label>
                                            <div class="layui-input-inline">
                                                <input type="text" id="L_email" name="email" value="<?php echo $Userfile['email']; ?>" disabled required lay-verify="email" autocomplete="off" value="" class="layui-input">
                                            </div>

                                            
                                            <?php if($Userfile['auth'] == 0): ?>
                                            <div class="layui-form-mid layui-word-aux">该邮箱未认证，需
                                                <a id="auth" style="font-size: 12px; color: #4f99cf;cursor: pointer;">验证邮箱</a>。
                                            </div>
                                            <?php endif; ?>
                                        </div>
                                    </th>
                                </thead>

                                <!-- 昵称 -->
                                <thead>
                                    <th>
                                        <div class="layui-form-Userfile">
                                            <label for="L_username" class="layui-form-label">昵称</label>
                                            <div class="layui-input-inline">
                                                <input type="text" id="L_username" name="nickname" value="<?php echo $Userfile['nickname']; ?>" required lay-verify="required" autocomplete="off" value="" class="layui-input">
                                            </div>

                                        </div>
                                    </th>
                                </thead>

                                <!-- 性别 -->
                                <thead>
                                    <th>
                                        <div class="layui-inline">
                                            <div class="layui-input-inline" style="width: 245px;">
                                                <input type="radio" name="sex" value="0" <?php echo $Userfile['sex']==0?'checked' : ''; ?> title="保密">保密
                                                <input type="radio" name="sex" value="1" <?php echo $Userfile['sex']==1?'checked' : ''; ?> title="男">男
                                                <input type="radio" name="sex" value="2" <?php echo $Userfile['sex']==2?'checked' : ''; ?> title="女">女
                                            </div>
                                        </div>
                                    </th>
                                </thead>

                                <!-- 地址 -->
                                <thead>
                                    <th>
                                        <div class="layui-form-Userfile">
                                            <!-- 省份 -->
                                            <label for="province" class="layui-form-label">省份</label>
                                            <div class="layui-input-inline">
                                                <select name="province" id="province" lay-filter="province">
                                                    <option value="0"><?php echo $Userfile['provinces']['name']; ?></option>
                                                    <?php foreach($provinces as $Userfile1): ?>
                                                    <option value="<?php echo $Userfile1['code']; ?>" <?php echo $Userfile1['code']==$Userfile1?'selected' : ''; ?>>
                                                        <?php echo $Userfile1['name']; ?>
                                                    </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>

                                            <!-- 市 -->
                                            <label for="city" class="layui-form-label">市</label>
                                            <div class="layui-input-inline">
                                                <select name="city" id="city" lay-filter="city">
                                                    <option value="0"><?php echo $Userfile['citys']['name']; ?></option>
                                                    <?php foreach($citys as $Userfile2): ?>
                                                    <option value="<?php echo $Userfile2['code']; ?>" <?php echo $Userfile1['code']==$Userfile2['city']?'selected' : ''; ?>>
                                                        <?php echo $Userfile2['name']; ?>
                                                    </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>

                                            <!-- 区 -->
                                            <label for="district" class="layui-form-label">区</label>
                                            <div class="layui-input-inline">
                                                <select name="district" id="district">
                                                    <option value="0"><?php echo $Userfile['districts']['name']; ?></option>
                                                    <?php foreach($districts as $Userfile1): ?>
                                                    <option value="<?php echo $Userfile1['code']; ?>"
                                                        <?php echo $Userfile1['code']==$Userfile1['district']?'selected' : ''; ?>>
                                                        <?php echo $Userfile1['name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </th>
                                </thead>

                                <!-- 头像 -->
                                <thead>
                                    <th>
                                        <div class="avatar-add">
                                            <p>建议尺寸168*168，支持jpg、png、gif，最大不能超过50KB</p>
                                            <button type="button" class="layui-btn upload-img" onclick="avatar.click()">
                                                <i class="layui-icon">&#xe67c;</i>选择头像
                                            </button>
                                            <input type="file" name="avatar" id="avatar" hidden>
                                            <img src="<?php echo !empty($Userfile['avatar'])?$Userfile['avatar'] : '/static/home/res/images/upload.png'; ?>" id="img">
                                            <span class="loading"></span>
                                        </div>
                                    </th>
                                </thead>

                                <!-- 签名 -->
                                <thead>
                                    <th>
                                        <div class="layui-form-Userfile layui-form-text">
                                            <label for="L_sign" class="layui-form-label">签名</label>
                                            <div class="layui-input-block">
                                                <textarea placeholder="随便写些什么刷下存在感" id="L_sign" name="content" autocomplete="off" class="layui-textarea" style="height: 80px;"><?php echo $Userfile['content']; ?></textarea>
                                            </div>
                                        </div>
                                    </th>
                                </thead>

                                <!-- 提交 -->
                                <thead>
                                    <th>
                                        <div class="layui-form-Userfile">
                                            <button class="layui-btn" key="set-mine" lay-submit>确认修改</button>
                                        </div>
                                    </th>
                                </thead>
                            </form>


                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="/static/admin/js/layui/layui.js"></script>
<script>
    layui.cache.page = '';
    layui.cache.user = {
        username: '游客'
        , uid: -1
        , avatar: '/static/home/res/images/avatar/00.jpg'
        , experience: 83
        , sex: '男'
    };
    layui.config({
        version: "3.0.0"
        , base: '/static/home/res/mods/' //这里实际使用时，建议改成绝对路径
    }).extend({
        fly: 'index'
    }).use('fly');

    layui.use(['layer', 'form'], function () {
        var $ = layui.jquery,
            layer = layui.layer,
            form = layui.form

        // 邮箱认证
        $('#auth').click(function () {
            let email = $('#L_email').val()

            // 发起异步请求
            $.ajax({
                type: 'post',
                url: `<?php echo url('home/user/email'); ?>`,
                data: {
                    email
                },
                dataType: 'json',
                success: function (res) {
                    if (res.code === 1) {
                        layer.msg(res.msg)
                    } else {
                        layer.msg(res.msg)
                    }
                }
            })
        })

        // 地址
        $('#province').change(function () {
            //   console.log($(this).val())

            let code = $(this).val()
            $.ajax({
                type: 'post',
                url: `<?php echo url('home/user/area'); ?>`,
                data: {
                    code
                },
                dataType: 'json',
                success: function (res) {
                    if (res.code === 1) {
                        let option = ''

                        res.data.forEach(Userfile => {
                            option += `<option value="${Userfile.code}">${Userfile.name}</option>`
                        })

                        $('#city').html(option)

                        let code = res.data[0].code ? res.data[0].code : ''

                        GetArea(code, "#district")

                        form.render('select')
                    }
                }
            })
        })

        $('#citys').change(function () {
            // console.log($(this).val())
            let code = $(this).val()

            GetArea(code, "#district")
        })


        function GetArea(code, elem) {
            $.ajax({
                type: 'post',
                url: `<?php echo url('home/user/area'); ?>`,
                data: {
                    code
                },
                dataType: 'json',
                success: function (res) {
                    if (res.code === 1) {
                        let option = ''

                        res.data.forEach(Userfile => {
                            option += `<option value="${Userfile.code}">${Userfile.name}</option>`
                        })

                        $(elem).html(option)

                        form.render('select')
                    }


                }
            })

        }

        // 图片
        function GetObjectURL(file) {
            // createObjectURL 给一个文件对象 可以提取出一个url本地地址出来
            var url = null;

            // 判断是否为IE浏览器还是google还是其他浏览器
            if (window.createObjectURL != undefined) {
                url = window.createObjectURL(file)
            } else if (window.URL != undefined) {
                url = window.URL.createObjectURL(file)
            } else if (window.webkitURL != undefined) {
                url = window.webkitURL.createObjectURL(file)
            }
            return url
        }
        $('#avatar').change(function () {
            let file = $(this)[0].files[0] ? $(this)[0].files[0] : {}
            let url = GetObjectURL(file)
            $('#img').attr('src', url)
        })

    })
</script>
        </div>
    </div>
</body>
</html>