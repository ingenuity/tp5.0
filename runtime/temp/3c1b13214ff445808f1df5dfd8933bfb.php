<?php if (!defined('THINK_PATH')) exit(); /*a:6:{s:73:"F:\!Abeifen\WWW\tp5.0\public/../application/home\view\index\register.html";i:1653558940;s:55:"F:\!Abeifen\WWW\tp5.0\application\home\view\layout.html";i:1653476814;s:60:"F:\!Abeifen\WWW\tp5.0\application\home\view\common\meta.html";i:1653471571;s:62:"F:\!Abeifen\WWW\tp5.0\application\home\view\common\script.html";i:1652864157;s:62:"F:\!Abeifen\WWW\tp5.0\application\home\view\common\header.html";i:1654352686;s:62:"F:\!Abeifen\WWW\tp5.0\application\home\view\common\footer.html";i:1652755560;}*/ ?>
<!-- 主页面 -->
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- 引用样式 -->
    <!--  -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>创领论坛</title>
<link rel="stylesheet" href="/static/home/res/layui/css/layui.css">
<link rel="stylesheet" href="/static/home/res/css/global.css">
<link rel="stylesheet" href="/static/home/res/css/common.css">
    <!-- 引用js  -->
    <script src="/static/home/res/layui/layui.js"></script>
<script>
layui.cache.page = '';
layui.cache.user = {
  username: '游客'
  ,uid: -1
  ,avatar: '/static/home/res/images/avatar/00.jpg'
  ,experience: 83
  ,sex: '男'
};
layui.config({
  version: "3.0.0"
  ,base: '/static/home/res/mods/' //这里实际使用时，建议改成绝对路径
}).extend({
  fly: 'index'
}).use('fly');
</script>

</head>
<body>
    <!-- 引用头部 -->
    <!-- 头部公共样式 -->
<div class="fly-header layui-bg-black">
    <div class="layui-container">
        <a class="fly-logo" href="<?php echo url('home/index/index'); ?>">
            <img src="/static/home/res/images/logo.png" alt="layui" />
        </a>

        <ul class="layui-nav fly-nav-user">
            <!-- 未登入的状态 -->
            <!-- <li class="layui-nav-item">
          <a class="iconfont icon-touxiang layui-hide-xs" href="user/login.html"></a>
        </li>
        <li class="layui-nav-item">
          <a href="user/login.html">登入</a>
        </li>
        <li class="layui-nav-item">
          <a href="user/reg.html">注册</a>
        </li>
        <li class="layui-nav-item layui-hide-xs">
          <a href="/app/qq/" onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})" title="QQ登入" class="iconfont icon-qq"></a>
        </li>
        <li class="layui-nav-item layui-hide-xs">
          <a href="/app/weibo/" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})" title="微博登入" class="iconfont icon-weibo"></a>
        </li> -->

            <!-- 登入后的状态 -->
            <?php if(\think\Request::instance()->action() != 'login' && \think\Request::instance()->action() !='register'): if($LoginUser): ?>
            <li class="layui-nav-item">
              <a href="<?php echo url('admin/index/index'); ?>">后台</a>
            </li>
            <li class="layui-nav-item">
                <a class="fly-nav-avatar" href="javascript:;">
                    <cite class="layui-hide-xs"><?php echo $LoginUser['nickname']; ?></cite>
                    <i class="iconfont icon-renzheng layui-hide-xs" title="认证信息：<?php echo $LoginUser['nickname']; ?>"></i>
                    <i class="layui-badge fly-badge-vip layui-hide-xs">VIP <?php echo $LoginUser['vip']; ?></i>
                    <img src="<?php echo $LoginUser['avatar_cdn']; ?>" alt="<?php echo $LoginUser['nickname']; ?>">
                </a>
                <dl class="layui-nav-child">
                    <dd>
                        <a href="<?php echo url('home/user/profile'); ?>"><i class="layui-icon">&#xe620;</i>基本资料</a>
                    </dd>
                    <dd>
                        <a href="<?php echo url('home/user/message'); ?>"><i class="iconfont icon-tongzhi" style="top: 4px"></i>我的消息</a>
                    </dd>
                    <dd>
                        <a href="<?php echo url('home/user/index'); ?>"><i class="layui-icon"
                                style="margin-left: 2px; font-size: 22px">&#xe68e;</i>我的主页</a>
                    </dd>
                    <hr style="margin: 5px 0" />
                    <dd><a href="<?php echo url('home/index/logout'); ?>" style="text-align: center">退出</a></dd>
                </dl>
            </li>
            <?php else: ?>
            <!-- 未登入的状态 -->
            <li class="layui-nav-item">
                <a class="iconfont icon-touxiang layui-hide-xs" href="<?php echo url('home/user/login'); ?>"></a>
            </li>
            <li class="layui-nav-item">
              <a href="<?php echo url('admin/index/index'); ?>">后台</a>
            </li>
            <li class="layui-nav-item">
                <a href="<?php echo url('home/index/login'); ?>">登入</a>
            </li>
            <li class="layui-nav-item">
                <a href="<?php echo url('home/index/register'); ?>">注册</a>
            </li>
            <!-- <li class="layui-nav-item layui-hide-xs">
        <a
          href="/app/qq/"
          onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})"
          title="QQ登入"
          class="iconfont icon-qq"
        ></a>
      </li>
      <li class="layui-nav-item layui-hide-xs">
        <a
          href="/app/weibo/"
          onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})"
          title="微博登入"
          class="iconfont icon-weibo">
        </a>
      </li> -->
            <?php endif; endif; ?>
        </ul>
    </div>
</div>
    
    <!-- 注册 -->
<div class="layui-container fly-marginTop">
    <div class="fly-panel fly-panel-user" pad20>
        <div class="layui-tab layui-tab-brief" lay-filter="user">
            <ul class="layui-tab-title">
                <li><a href="<?php echo url('home/index/login'); ?>">登入</a></li>
                <li class="layui-this">注册</li>
            </ul>
            <div class="layui-form layui-tab-content" id="LAY_ucm" style="padding: 20px 0;">
                <div class="layui-tab-item layui-show">
                    <div class="layui-form layui-form-pane">
                        <form method="post">
                            <div class="layui-form-item">
                                <label for="L_email" class="layui-form-label">邮箱</label>
                                <div class="layui-input-inline">
                                    <input type="text" id="L_email" name="email" placeholder="请输入邮箱" required lay-verify="email"
                                        autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-form-mid layui-word-aux">将会成为您唯一的登入名</div>
                            </div>
                            <div class="layui-form-item">
                                <label for="L_username" class="layui-form-label">昵称</label>
                                <div class="layui-input-inline">
                                    <input type="text" id="L_username" name="nickname" placeholder="请输入昵称" required lay-verify="required"
                                        autocomplete="off" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="L_pass" class="layui-form-label">密码</label>
                                <div class="layui-input-inline">
                                    <input type="password" id="L_pass" name="pass" placeholder="请输入密码" required lay-verify="required"
                                        autocomplete="off" class="layui-input">
                                </div>
                                <!-- <div class="layui-form-mid layui-word-aux">6到16个字符</div> -->
                            </div>
                            <div class="layui-form-item">
                                <label for="L_repass" class="layui-form-label">确认密码</label>
                                <div class="layui-input-inline">
                                    <input type="password" id="L_repass" name="repass" placeholder="请输入确认密码" required lay-verify="required"
                                        autocomplete="off" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="L_vercode" class="layui-form-label">验证码</label>
                                <div class="layui-input-inline">
                                    <input type="text" id="L_vercode" name="vercode" required lay-verify="required"
                                        placeholder="请回答后面的问题" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-form-mid">
                                    <img src="<?php echo url('home/index/vercode'); ?>" alt="captcha" onclick="this.src = `<?php echo url('home/index/vercode'); ?>`"  />
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <button class="layui-btn" lay-submit>立即注册</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    

    <!-- 引用底部 -->
    <div class="fly-footer">
    <p>2017 &copy; <a href="http://www.layui.com/" target="_blank">Fly技术社区</a></p>
</div>
</body>
</html>