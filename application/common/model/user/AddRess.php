<?php

namespace app\common\model\User;

use think\Model;

class AddRess extends Model
{
    //收货地址
    protected $table = 'pre_user_address';

    // 关联查询
    public function provinces()
    {
        /**
         * belongsTo 相对
         * 第一个参数 模型名
         * 第二个参数 关联外键
         * 第三个参数 关联主键
         * 第四个参数 模型别名
         * 第五个参数 链表方法（JOIN）
        */
        return $this->belongsTo('app\common\model\Region','province','code',[],'LEFT')->setEagerlyType(0);
    }

    public function citys()
    {
        /**
         * belongsTo 相对
         * 第一个参数 模型名
         * 第二个参数 关联外键
         * 第三个参数 关联主键
         * 第四个参数 模型别名
         * 第五个参数 链表方法（JOIN）
        */
        return $this->belongsTo('app\common\model\Region','city','code',[],'LEFT')->setEagerlyType(0);
    }

    public function districts()
    {
        /**
         * belongsTo 相对
         * 第一个参数 模型名
         * 第二个参数 关联外键
         * 第三个参数 关联主键
         * 第四个参数 模型别名
         * 第五个参数 链表方法（JOIN）
        */
        return $this->belongsTo('app\common\model\Region','district','code',[],'LEFT')->setEagerlyType(0);
    }
}
