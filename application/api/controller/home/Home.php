<?php

namespace app\api\controller\Home;

use think\Controller;
use think\Request;

class Home extends Controller
{
   public function index()
   {
    //    分类
    $TypeList = model('Product.Type')->order('weigh','desc')->limit(8)->select();
    // halt($TypeList);
    $NewLit = model('Product.Product')->where(['flag' => 1])->limit(4)->select();

    $data = [
        'TypeList' => $TypeList,
        'NewList' => $NewLit,
    ];
// halt($data);
// return $TypeList;
    $this->success('查询成功',null,$data);
   }
}
