<?php
// 订单详情
namespace app\common\model\product;

use think\Model;

class OrderProduct extends Model
{
    //订单商品表
    protected $table = 'pre_order_product';

    public function product()
    {
        return $this->belongsTo('app\common\model\Product\Product','proid','id',[])->setEagerlyType(0);
    }

    public function order()
    {
        return $this->belongsTo('app\common\model\Product\Order','orderid','id',[])->setEagerlyType(0);
    }
}
