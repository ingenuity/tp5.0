<?php if (!defined('THINK_PATH')) exit(); /*a:6:{s:69:"F:\!Abeifen\WWW\tp5.0\public/../application/home\view\user\index.html";i:1653566511;s:55:"F:\!Abeifen\WWW\tp5.0\application\home\view\layout.html";i:1653476814;s:60:"F:\!Abeifen\WWW\tp5.0\application\home\view\common\meta.html";i:1653471571;s:62:"F:\!Abeifen\WWW\tp5.0\application\home\view\common\script.html";i:1652864157;s:62:"F:\!Abeifen\WWW\tp5.0\application\home\view\common\header.html";i:1654352686;s:62:"F:\!Abeifen\WWW\tp5.0\application\home\view\common\footer.html";i:1652755560;}*/ ?>
<!-- 主页面 -->
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- 引用样式 -->
    <!--  -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>创领论坛</title>
<link rel="stylesheet" href="/static/home/res/layui/css/layui.css">
<link rel="stylesheet" href="/static/home/res/css/global.css">
<link rel="stylesheet" href="/static/home/res/css/common.css">
    <!-- 引用js  -->
    <script src="/static/home/res/layui/layui.js"></script>
<script>
layui.cache.page = '';
layui.cache.user = {
  username: '游客'
  ,uid: -1
  ,avatar: '/static/home/res/images/avatar/00.jpg'
  ,experience: 83
  ,sex: '男'
};
layui.config({
  version: "3.0.0"
  ,base: '/static/home/res/mods/' //这里实际使用时，建议改成绝对路径
}).extend({
  fly: 'index'
}).use('fly');
</script>

</head>
<body>
    <!-- 引用头部 -->
    <!-- 头部公共样式 -->
<div class="fly-header layui-bg-black">
    <div class="layui-container">
        <a class="fly-logo" href="<?php echo url('home/index/index'); ?>">
            <img src="/static/home/res/images/logo.png" alt="layui" />
        </a>

        <ul class="layui-nav fly-nav-user">
            <!-- 未登入的状态 -->
            <!-- <li class="layui-nav-item">
          <a class="iconfont icon-touxiang layui-hide-xs" href="user/login.html"></a>
        </li>
        <li class="layui-nav-item">
          <a href="user/login.html">登入</a>
        </li>
        <li class="layui-nav-item">
          <a href="user/reg.html">注册</a>
        </li>
        <li class="layui-nav-item layui-hide-xs">
          <a href="/app/qq/" onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})" title="QQ登入" class="iconfont icon-qq"></a>
        </li>
        <li class="layui-nav-item layui-hide-xs">
          <a href="/app/weibo/" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})" title="微博登入" class="iconfont icon-weibo"></a>
        </li> -->

            <!-- 登入后的状态 -->
            <?php if(\think\Request::instance()->action() != 'login' && \think\Request::instance()->action() !='register'): if($LoginUser): ?>
            <li class="layui-nav-item">
              <a href="<?php echo url('admin/index/index'); ?>">后台</a>
            </li>
            <li class="layui-nav-item">
                <a class="fly-nav-avatar" href="javascript:;">
                    <cite class="layui-hide-xs"><?php echo $LoginUser['nickname']; ?></cite>
                    <i class="iconfont icon-renzheng layui-hide-xs" title="认证信息：<?php echo $LoginUser['nickname']; ?>"></i>
                    <i class="layui-badge fly-badge-vip layui-hide-xs">VIP <?php echo $LoginUser['vip']; ?></i>
                    <img src="<?php echo $LoginUser['avatar_cdn']; ?>" alt="<?php echo $LoginUser['nickname']; ?>">
                </a>
                <dl class="layui-nav-child">
                    <dd>
                        <a href="<?php echo url('home/user/profile'); ?>"><i class="layui-icon">&#xe620;</i>基本资料</a>
                    </dd>
                    <dd>
                        <a href="<?php echo url('home/user/message'); ?>"><i class="iconfont icon-tongzhi" style="top: 4px"></i>我的消息</a>
                    </dd>
                    <dd>
                        <a href="<?php echo url('home/user/index'); ?>"><i class="layui-icon"
                                style="margin-left: 2px; font-size: 22px">&#xe68e;</i>我的主页</a>
                    </dd>
                    <hr style="margin: 5px 0" />
                    <dd><a href="<?php echo url('home/index/logout'); ?>" style="text-align: center">退出</a></dd>
                </dl>
            </li>
            <?php else: ?>
            <!-- 未登入的状态 -->
            <li class="layui-nav-item">
                <a class="iconfont icon-touxiang layui-hide-xs" href="<?php echo url('home/user/login'); ?>"></a>
            </li>
            <li class="layui-nav-item">
              <a href="<?php echo url('admin/index/index'); ?>">后台</a>
            </li>
            <li class="layui-nav-item">
                <a href="<?php echo url('home/index/login'); ?>">登入</a>
            </li>
            <li class="layui-nav-item">
                <a href="<?php echo url('home/index/register'); ?>">注册</a>
            </li>
            <!-- <li class="layui-nav-item layui-hide-xs">
        <a
          href="/app/qq/"
          onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})"
          title="QQ登入"
          class="iconfont icon-qq"
        ></a>
      </li>
      <li class="layui-nav-item layui-hide-xs">
        <a
          href="/app/weibo/"
          onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})"
          title="微博登入"
          class="iconfont icon-weibo">
        </a>
      </li> -->
            <?php endif; endif; ?>
        </ul>
    </div>
</div>
    
    <!-- 我的主页的html -->
<div class="fly-home fly-panel" style=" background-image: url(<?php echo $LoginUser['cover_cdn']; ?>);">
    <img src="<?php echo $LoginUser['avatar_cdn']; ?>" alt="<?php echo $LoginUser['nickname']; ?>">
    <?php if($LoginUser['auth'] == 1): ?>
    <i class="iconfont icon-renzheng" title="Fly社区认证"></i>
    <?php endif; ?>
    <h1 style="color: #fff;">
        <?php echo $LoginUser['nickname']; ?>

        <!-- 判断男女后输出视图 -->
        <?php if($LoginUser['sex'] == 1): ?>
        <i class="iconfont icon-nan"></i>
        <?php elseif($LoginUser['sex'] == 2): ?>
        <i class="iconfont icon-nv"></i>
        <?php else: ?>
        <span><?php echo $LoginUser['sex_text']; ?></span>
        <?php endif; ?>

        <!-- Vip等级视图输出 -->
        <i class="layui-badge fly-badge-vip">VIP <?php echo $LoginUser['vip']; ?></i>
        <!--
      <span style="color:#c00;">（管理员）</span>
      <span style="color:#5FB878;">（社区之光）</span>
      <span>（该号已被封）</span>
      -->
    </h1>

    <!-- 判断是否有邮箱认证并输出视图 -->
    <?php if($LoginUser['auth']): ?>
    <p style="padding: 10px 0; color: #5FB878;">认证信息：<?php echo $LoginUser['nickname']; ?></p>
    <?php else: ?>
    <p style="color:#c00;"> 未认证 </p>
    <?php endif; ?>

    <p class="fly-home-info">

        <!-- 输出数据库里的积分 -->
        <i class="iconfont icon-kiss" title="飞吻"></i><span style="color: #FF7200;"><?php echo $LoginUser['point']; ?> 积分</span>

        <!-- 输出创建账号时间 -->
        <i class="iconfont icon-shijian"></i><span><?php echo $LoginUser['createtime']; ?> 加入</span>

        <!-- 判断资料里的地址，输出视图 -->
        <?php if($LoginUser['province']): ?>
        <?php echo $LoginUser['provinces']['name']; ?>-<?php echo $LoginUser['citys']['name']; ?>-<?php echo $LoginUser['districts']['name']; else: ?>
        <i class="iconfont icon-chengshi"></i><span>保密</span>
        <?php endif; ?>
    </p>

    <p class="fly-home-sign">（<?php echo !empty($LoginUser['content'])?$LoginUser['content'] : '人生仿若一场游戏'; ?>）</p>

    <div class="fly-sns" data-user="">
        <a href="javascript:;" class="layui-btn layui-btn-primary fly-imActive" data-type="addFriend">加为好友</a>
        <a href="javascript:;" class="layui-btn layui-btn-normal fly-imActive" data-type="chat">发起会话</a>
    </div>

</div>

<div class="layui-container">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md6 fly-home-jie">
            <div class="fly-panel">
                <?php foreach($LoginPostlist as $item): ?>
                <h3 class="fly-panel-title"><?php echo $LoginUser['nickname']; ?> 最近的提问</h3>
                <ul class="jie-row">
                    <li>
                        <span class="fly-jing">精</span>
                        <a href="" class="jie-title"><?php echo $item['content']; ?>12345上山打老虎</a>
                        <i><?php echo $item['createtime']; ?></i>
                        <em class="layui-hide-xs">1136阅/27答</em>
                    </li>
                    <li>
                        <a href="" class="jie-title"><?php echo $item['content']; ?> 老虎打不到</a>
                        <i><?php echo $item['createtime']; ?></i>
                        <em class="layui-hide-xs">1136阅/27答</em>
                    </li>

                    <!-- <div class="fly-none" style="min-height: 50px; padding:30px 0; height:auto;"><i style="font-size:14px;">没有发表任何求解</i></div> -->
                </ul>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="layui-col-md6 fly-home-da">
            <div class="fly-panel">
                <h3 class="fly-panel-title"><?php echo $LoginUser['nickname']; ?> 最近的回答</h3>
                <ul class="home-jieda">
                    <li>
                        <p>
                            <span><?php echo $LoginUser['createtime']; ?></span>
                            在<a href="" target="_blank">tips能同时渲染多个吗?</a>中回答：
                        </p>
                        <div class="home-dacontent">
                            尝试给layer.photos加上这个属性试试：
                            <pre>  full: true </pre>
                            文档没有提及
                        </div>
                    </li>
                    <li>
                        <p>
                            <span><?php echo $LoginUser['createtime']; ?></span>
                            在<a href="" target="_blank">在Fly社区用的是什么系统啊?</a>中回答：
                        </p>
                        <div class="home-dacontent">
                            Fly社区采用的是NodeJS。分享出来的只是前端模版
                        </div>
                    </li>

                    <!-- <div class="fly-none" style="min-height: 50px; padding:30px 0; height:auto;"><span>没有回答任何问题</span></div> -->
                </ul>
            </div>
        </div>
    </div>
</div>
    

    <!-- 引用底部 -->
    <div class="fly-footer">
    <p>2017 &copy; <a href="http://www.layui.com/" target="_blank">Fly技术社区</a></p>
</div>
</body>
</html>