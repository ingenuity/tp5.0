<?php

namespace app\common\validate\Post;

//引入底层的验证器类
use think\Validate;

/**
 * 帖子评论验证器
 */
class Comment extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'pid' => 'require',
        'content' => 'require',
        'userid' => 'require',
        'postid' => 'require',
        'accept' => 'number|in:0,1',  //给字段设置范围
    ];

    /**
     * 提示消息
     */
    protected $message = [
        'pid.require' => '评论的上级信息未知',
        'content.require' => '请填写评论的内容',
        'userid.require' => '用户信息未知',
        'postid.require' => '帖子信息未知',
        'accept.number' => '采纳状态必须是一个数值',
    ];
    
    /**
     * 验证场景
     */
    protected $scene = [
    ];
}