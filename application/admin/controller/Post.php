<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Controller;
use think\Request;

class Post extends Backend
{
    public function __construct()
    {
        parent::__construct();
        $this->PostModel = model('Post.Post');
    }

    public function index()
    {
        // 获取所有用户数据  分页
        $PostList = $this->PostModel->with('cate', 'user')->order('createtime')->paginate(4, false, ['query' => Request::instance()->param()]);
        // preg_match_all("/<h[1-5]>(.*?)<\/h[1-5]>/",$item['content'],$res);

        return $this->fetch('', [
            'PostList' => $PostList
        ]);
    }

    // 删除
    public function del() {
        $postid = $this->request->param('id');
        $post = $this->PostModel->find($postid);
        if (!$post) {
            $this->error('无法删除该帖子');
        }
        $res = $this->PostModel->destroy($postid);
        if ($res == false) {
            $this->error('删除帖子失败');
        }
        $this->success('删除帖子成功');
    }

    // 批量删除
    public function delAll() {
        $action = $this->request->param('action');
        if ($action == 'delAll') {
            $params = $this->request->param();
            $ids = implode(',', $params['list']);
            // halt($ids);
            // $res = $this->PostModel->where("id in ($ids)")->delete();
            $res = $this->PostModel->destroy($ids);
            // halt($res);
            if (!$res) {
                $this->error('批量删除失败');
            } 
            $this->success("批量删除成功", null, null, 2);
        }
    }

    // 回收站
    public function recovery() {
        $RecoveryList = model('Post.Post')->onlyTrashed()->order('createtime', 'desc')->paginate(5, false, ['query' => Request::instance()->param()]);

        if ($this->request->isAjax()) {
            $action = $this->request->param('action');
            // 彻底删除
            if ($action == 'del') {
                $id = $this->request->param('id');
                $post = $this->PostModel->onlyTrashed()->find($id);
                if ($post == null) {
                    $this->error('该帖子无法彻底删除');
                }
                // halt('123');
                $res = $this->PostModel->destroy($id,true);
                if ($res == false) {
                    $this->error('删除失败');
                }
                $this->success('该帖子已经彻底删除');
            } 
            
            // 恢复
            if ($action == 'return') {
                $id = $this->request->param('id');
                $post = $this->PostModel->onlyTrashed()->find($id);
                if ($post == null) {
                    $this->error('该帖子无法恢复');
                }
                // $this->success('该帖子可以恢复');
                $data = [
                    'id' => $id,
                    'deletetime' => null
                ];
                $res = $this->PostModel->isUpdate(true)->save($data);
                if ($res == false) {
                    $this->error('恢复失败');
                }
                $this->success('恢复成功');
            }

            // 批量删除
            if ($action == 'delAll') {
                $params = $this->request->param();
                $ids = implode(',', $params['list']);
                // halt($ids);
                $data = [
                    'id' => $id,
                    'deletetime' => null
                ];
                $res = $this->PostModel->destroy($ids, true);
                if (!$res) {
                    $this->error('批连删除失败');
                } 
                $this->success("批量删除成功", null, null, 2);
            } 

            // 批量恢复
            if ($action == 'returnAll') {
                $params = $this->request->param();
                $ids = implode(',', $params['list']);
                $data = [];
                foreach($params['list'] as $key => $item) {
                    $arr['id'] = $item;
                    $arr['deletetime'] = null;
                    array_push($data, $arr);
                }
                // halt($data);
                $res = $this->PostModel->isUpdate(true)->saveAll($data);
                if (!$res) {
                    $this->error('批量恢复失败');
                } 
                $this->success("批量恢复成功", null, null, 2);
            }
        }

        return $this->fetch('', [
            'RecoveryList' => $RecoveryList,
        ]);
    }
}
