<?php

namespace app\home\controller;

use app\common\controller\Home;

// 继承home
class Comment extends Home
{
    // 创建一个构造函数，来继承父级
    public function __construct()
    {
        // parent：父级  __construct()：构造函数
        parent::__construct();

        // 实例这个application\common\model\post\Comment.php表
        $this->CommentModel = model('Post.Comment');

        $this->UserModel = model('User.User');

        $this->PostModel = model('Post.Post');

        $this->RecordModel = model('User.Record');
    }

    // 创建一个add方法
    public function add()
    {
        // 判断该页面里面的post是否有提交 $this->request->isPost()：在接收空数据的
        if($this->request->isPost())
        {
            // 接收评论内容 $this->request->param():是接收多个参数，在括号内用单引号来圈出接收的name
            $content = $this->request->param('content');

            // 接收用户id isset:是判断  判断是否有id没有就给0
            $userid = isset($this->LoginUser['id']) ? $this->LoginUser['id'] : 0;

            // 接收评论的上一级id 设置默认值为0
            $pid = $this->request->param('postid',0);

            // 获取帖子id
            $postid = $this->request->param('postid');

            // 连接application\common\model\post\Post.php查询里面是数据库的$postid；find() 方法查询结果不存在，返回 null
            $post = model('Post.Post')->find($postid);
            
            if(!$post)
            {
                $this->error('当前评论帖子不存在');
            }

            // 封装数据
            $data = [
                'content' => $content,
                'userid' => $userid,
                'pid' => $pid,
                'accept' => 0,
                'postid' => $postid
            ];
            // halt($data);
            $comment = $this->CommentModel->find($pid);

            if($comment)
            {
                $data['level'] = $comment['level'] + 1;
            }else{
                $data['level'] = 0;
            }

            $result = $this->CommentModel->validate('common/Post/Comment')->save($data);


            if($result === FALSE)
            {
                $this->error($this->CommentModel->getError());
            }else{
                $this->success('评论成功');
            }
        }
    }

    // 采纳
    public function accept()
    {
        if($this->request->isAjax())
        {
            $comid = $this->request->param('comid');
            
            $comment = $this->CommentModel->with('post')->find($comid);

            if(!$comment)
            {
                $this->error('该评论不存在，无法采纳');
            }

            /* 
                评论表 => accept = 1
                帖子表 => accept = userid
                用户表 => accept => userid = 加积分
                消费记录表 => accept => userid = 新增记录
            */

            // 开启事务 => 操作多个表 => 更新或者新增 => 当某个表新增或者更新失败，它可以让操作的表撤回新增或者更新的数据
            $this->CommentModel->startTrans();
            $this->UserModel->startTrans();
            $this->PostModel->startTrans();
            $this->RecordModel->startTrans();

            // 更新评论表数据
            $CommentData = [
                'id' => $comid,
                'accept' => 1
            ];

            $CommentStatus = $this->CommentModel->isUpdate(true)->save($CommentData);

            if($CommentStatus === FALSE)
            {
                $this->error($this->CommentModel->getError());
            }

            // 获取悬赏积分 => 获取用户的积分
            $User = $this->userModel->find($comment['userid']);

            if(!$User)
            {
                $this->error('该用户不存在');
            }

            $UpdatePonit = bcadd($User['point'],$comment['post']['point']);

            $UserData = [
                'id' => $comment['userid'],
                'point' => $UpdatePonit
            ];

            $UserStatus = $this->UserModel->isUpdate(true)->save($UserData);

            if($UserStatus === FALSE)
            {
                $this->CommentModel->rollback();
                $this->error($this->UserModel->getError);
            }

            // 帖子
            $PostData = [
                'id' => $comment['postid'],
                'accept' => $comment['userid']
            ];

            $PostStatus = $this->PostModel->isUpdate(true)->save($PostData);

            if($PostStatus ===FALSE)
            {
                $this->CommentModel->rollback();
                $this->UserModel->rollback();
                $this->error($this->PostModel->getError);
            }

            // 消费记录
            $RecordData = [
                'point' => $comment['post']['point'],
                'content' => "您在【{$comment['post']['title']}】里评论内容被采用，获得了{$comment['post']['point']}积分",
                'state' => 2,
                'userid' => $comment['userid']
            ];

            $RecordStatus = $this->RecordModel->validate('common/User/Record')->save($RecordData);

            if($RecordStatus === FALSE)
            {
                $this->CommentModel->rollback();
                $this->UserModel->rollback();
                $this->PostModel->rollback();
                $this->error($this->RecordModel->getError);
            }

            if($RecordStatus === FALSE || $CommentStatus === FALSE || $UserStatus === FALSE || $PostStatus === FALSE)
            {
                $this->CommentModel->rollback();
                $this->UserModel->rollback();
                $this->PostModel->rollback();
                $this->RecordModel->rollback();
                $this->error('采纳失败');
            }else{
                $this->CommentModel->commit();
                $this->UserModel->commit();
                $this->PostModel->commit();
                $this->RecordModel->commit();
                $this->success('采纳成功');
            }
        }
    }
}
