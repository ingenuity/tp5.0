<?php

namespace app\common\validate\Admin;

use think\Validate;

// 管理员验证器
class Admin extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'username' => 'require', //必填
        'password' => 'require', //必填
        'salt' => 'require', //必填
        'nickname' => 'require', //必填
        'state' => 'in:0,1',  //给字段设置范围
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'username.require' => '用户名称必填',
        'username.unique' => '该用户名称已注册，请重新填写',
        'nickname.require'  => '昵称必填',
        'password.require' => '密码必填',
        'salt.require' => '密码盐生成有误，请稍后重试',
        'state.in' => '选择的状态有误，请重新选择',
    ];
}