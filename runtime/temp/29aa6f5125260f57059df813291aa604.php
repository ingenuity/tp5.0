<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:71:"F:\!Abeifen\WWW\tp5.0\public/../application/admin\view\index\login.html";i:1653906241;s:61:"F:\!Abeifen\WWW\tp5.0\application\admin\view\common\meta.html";i:1653968148;s:63:"F:\!Abeifen\WWW\tp5.0\application\admin\view\common\script.html";i:1653894646;}*/ ?>
<!DOCTYPE html>
<html lang="zh">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="keywords" content="LightYear,LightYearAdmin,光年,后台模板,后台管理系统,光年HTML模板">
<meta name="description" content="Light Year Admin V4是一个基于Bootstrap v4.4.1的后台管理系统的HTML模板。">
<meta name="author" content="yinq">
<title><?php echo $system['SiteName']; ?>后台管理系统</title>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo $system['favicon']; ?>">
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<link rel="stylesheet" type="text/css" href="/static/admin/css/materialdesignicons.min.css">
<link rel="stylesheet" type="text/css" href="/static/admin/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/static/admin/css/animate.min.css">
<link rel="stylesheet" type="text/css" href="/static/admin/css/style.min.css">
<style>
.login-box {
    background-color: rgba(255, 255, 255, .25);
}
.login-box p:last-child {
    margin-bottom: 0px;
}
.login-form .form-control {
    background: rgba(0, 0, 0, 0.3);
    color: #fff;
}
.login-form .has-feedback {
    position: relative;
}
.login-form .has-feedback .form-control {
    padding-left: 36px;
}
.login-form .has-feedback .mdi {
    position: absolute;
    top: 0;
    left: 0;
    right: auto;
    width: 36px;
    height: 36px;
    line-height: 36px;
    z-index: 4;
    color: #dcdcdc;
    display: block;
    text-align: center;
    pointer-events: none;
}
.login-form .has-feedback.row .mdi {
    left: 15px;
}
.login-form .form-control::-webkit-input-placeholder{ 
    color: rgba(255, 255, 255, .8);
} 
.login-form .form-control:-moz-placeholder{ 
    color: rgba(255, 255, 255, .8);
} 
.login-form .form-control::-moz-placeholder{ 
    color: rgba(255, 255, 255, .8);
} 
.login-form .form-control:-ms-input-placeholder{ 
    color: rgba(255, 255, 255, .8);
}
.login-form .custom-control-label::before {
    background: rgba(0, 0, 0, 0.3);
    border-color: rgba(0, 0, 0, 0.1);
}
</style>
</head>
  
<body class="center-vh" style="background-image: url(/static/admin/images/login-bg-5.png); background-size: cover;">

<!--页面loading end-->
<div class="login-box p-5 w-420 mb-0 mr-2 ml-2">
  <div class="text-center mb-3">
    <a href="index.html"> <img alt="light year admin" src="/static/admin/images/logo-sidebar.png"> </a>
  </div>
  <form method="post" class="login-form">
    <div class="form-group has-feedback">
      <span class="mdi mdi-account" aria-hidden="true"></span>
      <input type="text" class="form-control" name="username" id="username" placeholder="用户名">
    </div>

    <div class="form-group has-feedback">
      <span class="mdi mdi-lock" aria-hidden="true"></span>
      <input type="password" class="form-control" name="password" id="password" placeholder="密码">
    </div>
    
    <div class="form-group has-feedback row">
      <div class="col-7">
        <span class="mdi mdi-check-all form-control-feedback" aria-hidden="true"></span>
        <input type="text" name="captcha" class="form-control" placeholder="验证码">
      </div>
      <div class="col-5 text-right">
        <img src="<?php echo url('home/index/vercode'); ?>" alt="captcha" onclick="this.src =`<?php echo url('home/index/vercode'); ?>` " />
      </div>
    </div>

    <div class="form-group">
      <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="rememberme" value="1" name="rememberme">
        <label class="custom-control-label not-user-select text-white" for="rememberme">5天内自动登录</label>
      </div>
    </div>

    <div class="form-group">
      <button class="btn btn-block btn-primary" type="submit">立即登录</button>
    </div>
  </form>
  
  <p class="text-center text-white">Copyright © 2020 <a href="http://lyear.itshubao.com">IT书包</a>. All right reserved</p>
</div>

</body>
</html>
<script type="text/javascript" src="/static/admin/js/jquery.min.js"></script>
<script type="text/javascript" src="/static/admin/js/popper.min.js"></script>
<script type="text/javascript" src="/static/admin/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/static/admin/js/jquery.cookie.min.js"></script>
<script type="text/javascript" src="/static/admin/js/main.min.js"></script>
<script type="text/javascript" src="/static/admin/js/Chart.min.js"></script>