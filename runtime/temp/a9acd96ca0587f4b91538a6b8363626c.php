<?php if (!defined('THINK_PATH')) exit(); /*a:7:{s:68:"F:\!Abeifen\WWW\tp5.0\public/../application/home\view\user\home.html";i:1653470370;s:55:"F:\!Abeifen\WWW\tp5.0\application\home\view\layout.html";i:1653476814;s:60:"F:\!Abeifen\WWW\tp5.0\application\home\view\common\meta.html";i:1653471571;s:62:"F:\!Abeifen\WWW\tp5.0\application\home\view\common\script.html";i:1652864157;s:62:"F:\!Abeifen\WWW\tp5.0\application\home\view\common\header.html";i:1654352686;s:60:"F:\!Abeifen\WWW\tp5.0\application\home\view\common\menu.html";i:1653471554;s:62:"F:\!Abeifen\WWW\tp5.0\application\home\view\common\footer.html";i:1652755560;}*/ ?>
<!-- 主页面 -->
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- 引用样式 -->
    <!--  -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>创领论坛</title>
<link rel="stylesheet" href="/static/home/res/layui/css/layui.css">
<link rel="stylesheet" href="/static/home/res/css/global.css">
<link rel="stylesheet" href="/static/home/res/css/common.css">
    <!-- 引用js  -->
    <script src="/static/home/res/layui/layui.js"></script>
<script>
layui.cache.page = '';
layui.cache.user = {
  username: '游客'
  ,uid: -1
  ,avatar: '/static/home/res/images/avatar/00.jpg'
  ,experience: 83
  ,sex: '男'
};
layui.config({
  version: "3.0.0"
  ,base: '/static/home/res/mods/' //这里实际使用时，建议改成绝对路径
}).extend({
  fly: 'index'
}).use('fly');
</script>

</head>
<body>
    <!-- 引用头部 -->
    <!-- 头部公共样式 -->
<div class="fly-header layui-bg-black">
    <div class="layui-container">
        <a class="fly-logo" href="<?php echo url('home/index/index'); ?>">
            <img src="/static/home/res/images/logo.png" alt="layui" />
        </a>

        <ul class="layui-nav fly-nav-user">
            <!-- 未登入的状态 -->
            <!-- <li class="layui-nav-item">
          <a class="iconfont icon-touxiang layui-hide-xs" href="user/login.html"></a>
        </li>
        <li class="layui-nav-item">
          <a href="user/login.html">登入</a>
        </li>
        <li class="layui-nav-item">
          <a href="user/reg.html">注册</a>
        </li>
        <li class="layui-nav-item layui-hide-xs">
          <a href="/app/qq/" onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})" title="QQ登入" class="iconfont icon-qq"></a>
        </li>
        <li class="layui-nav-item layui-hide-xs">
          <a href="/app/weibo/" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})" title="微博登入" class="iconfont icon-weibo"></a>
        </li> -->

            <!-- 登入后的状态 -->
            <?php if(\think\Request::instance()->action() != 'login' && \think\Request::instance()->action() !='register'): if($LoginUser): ?>
            <li class="layui-nav-item">
              <a href="<?php echo url('admin/index/index'); ?>">后台</a>
            </li>
            <li class="layui-nav-item">
                <a class="fly-nav-avatar" href="javascript:;">
                    <cite class="layui-hide-xs"><?php echo $LoginUser['nickname']; ?></cite>
                    <i class="iconfont icon-renzheng layui-hide-xs" title="认证信息：<?php echo $LoginUser['nickname']; ?>"></i>
                    <i class="layui-badge fly-badge-vip layui-hide-xs">VIP <?php echo $LoginUser['vip']; ?></i>
                    <img src="<?php echo $LoginUser['avatar_cdn']; ?>" alt="<?php echo $LoginUser['nickname']; ?>">
                </a>
                <dl class="layui-nav-child">
                    <dd>
                        <a href="<?php echo url('home/user/profile'); ?>"><i class="layui-icon">&#xe620;</i>基本资料</a>
                    </dd>
                    <dd>
                        <a href="<?php echo url('home/user/message'); ?>"><i class="iconfont icon-tongzhi" style="top: 4px"></i>我的消息</a>
                    </dd>
                    <dd>
                        <a href="<?php echo url('home/user/index'); ?>"><i class="layui-icon"
                                style="margin-left: 2px; font-size: 22px">&#xe68e;</i>我的主页</a>
                    </dd>
                    <hr style="margin: 5px 0" />
                    <dd><a href="<?php echo url('home/index/logout'); ?>" style="text-align: center">退出</a></dd>
                </dl>
            </li>
            <?php else: ?>
            <!-- 未登入的状态 -->
            <li class="layui-nav-item">
                <a class="iconfont icon-touxiang layui-hide-xs" href="<?php echo url('home/user/login'); ?>"></a>
            </li>
            <li class="layui-nav-item">
              <a href="<?php echo url('admin/index/index'); ?>">后台</a>
            </li>
            <li class="layui-nav-item">
                <a href="<?php echo url('home/index/login'); ?>">登入</a>
            </li>
            <li class="layui-nav-item">
                <a href="<?php echo url('home/index/register'); ?>">注册</a>
            </li>
            <!-- <li class="layui-nav-item layui-hide-xs">
        <a
          href="/app/qq/"
          onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})"
          title="QQ登入"
          class="iconfont icon-qq"
        ></a>
      </li>
      <li class="layui-nav-item layui-hide-xs">
        <a
          href="/app/weibo/"
          onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})"
          title="微博登入"
          class="iconfont icon-weibo">
        </a>
      </li> -->
            <?php endif; endif; ?>
        </ul>
    </div>
</div>
    
    <!-- 用户中心查询 -->
<!-- 我发的贴，我收藏的贴 -->
<div class="layui-container fly-marginTop fly-user-main">
  <!-- 主页右上角下拉框样式 -->
<ul class="layui-nav layui-nav-tree layui-inline" lay-filter="user">
    <li class="layui-nav-item">
      <a href="<?php echo url('home/user/index'); ?>">
        <i class="layui-icon">&#xe609;</i>
        我的主页
      </a>
    </li>
    <li class="layui-nav-item <?php echo $navTitle=='用户中心'?'layui-this' : ''; ?>">
      <a href="<?php echo url('home/user/home'); ?>">
        <i class="layui-icon">&#xe612;</i>
        用户中心
      </a>
    </li>
    <li class="layui-nav-item <?php echo $navTitle=='基本资料'?'layui-this' : ''; ?>">
      <a href="<?php echo url('home/user/profile'); ?>">
        <i class="layui-icon">&#xe620;</i>
        基本资料
      </a>
    </li>
    <li class="layui-nav-item <?php echo $navTitle=='消息列表'?'layui-this' : ''; ?>">
      <a href="<?php echo url('home/user/message'); ?>">
        <i class="layui-icon">&#xe620;</i>
        消息列表
      </a>
    </li>
    <li class="layui-nav-item <?php echo $navTitle=='积分消费中心'?'layui-this' : ''; ?>">
      <a href="<?php echo url('home/user/record'); ?>">
        <i class="layui-icon">&#xe611;</i>
        积分消费记录
      </a>
    </li>
  </ul>

  <div class="site-tree-mobile layui-hide">
    <i class="layui-icon">&#xe602;</i>
  </div>
  <div class="site-mobile-shade"></div>
  
  <div class="site-tree-mobile layui-hide">
    <i class="layui-icon">&#xe602;</i>
  </div>
  <div class="site-mobile-shade"></div>
  <div class="fly-panel fly-panel-user" pad20>
  <!--
      <div class="fly-msg" style="margin-top: 15px;">
        您的邮箱尚未验证，这比较影响您的帐号安全，<a href="activate.html">立即去激活？</a>
      </div>
      -->
  <div class="layui-tab layui-tab-brief" lay-filter="user">
    <ul class="layui-tab-title" id="LAY_mine">
      <li data-type="mine-jie" lay-id="index" class="layui-this">我发的帖（<span>89</span>）</li>
      <li data-type="collection" data-url="/collection/find/" lay-id="collection">我收藏的帖（<span>16</span>）</li>
    </ul>
    <div class="layui-tab-content" style="padding: 20px 0;">
      <div class="layui-tab-item layui-show">
        <ul class="mine-view jie-row">
          <li>
            <a class="jie-title" href="../jie/detail.html" target="_blank">基于 layui 的极简社区页面模版</a>
            <i>2017/3/14 上午8:30:00</i>
            <a class="mine-edit" href="/jie/edit/8116">编辑</a>
            <em>661阅/10答</em>
          </li>
          <li>
            <a class="jie-title" href="../jie/detail.html" target="_blank">基于 layui 的极简社区页面模版</a>
            <i>2017/3/14 上午8:30:00</i>
            <a class="mine-edit" href="/jie/edit/8116">编辑</a>
            <em>661阅/10答</em>
          </li>
          <li>
            <a class="jie-title" href="../jie/detail.html" target="_blank">基于 layui 的极简社区页面模版</a>
            <i>2017/3/14 上午8:30:00</i>
            <a class="mine-edit" href="/jie/edit/8116">编辑</a>
            <em>661阅/10答</em>
          </li>
        </ul>
        <div id="LAY_page"></div>
      </div>
      <div class="layui-tab-item">
        <ul class="mine-view jie-row">
          <li>
            <a class="jie-title" href="../jie/detail.html" target="_blank">基于 layui 的极简社区页面模版</a>
            <i>收藏于23小时前</i>
          </li>
        </ul>
        <div id="LAY_page1"></div>
      </div>
    </div>
  </div>
</div>
</div>
    

    <!-- 引用底部 -->
    <div class="fly-footer">
    <p>2017 &copy; <a href="http://www.layui.com/" target="_blank">Fly技术社区</a></p>
</div>
</body>
</html>