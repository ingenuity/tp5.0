<?php
namespace app\common\validate\User;
//引入底层的验证器
use think\Validate;

/**
 * 用户消费记录验证器
 */
class record extends Validate
{
    /**
     * 验证规则
     */
    //状态消费积分状态
    // 1、发布
    // 2、采纳
    // 3、充值
    // 4、签到
    // 5、下商品订单
    // 6、下预约订单
    // -1、所有类型退款
    protected $rule = [
        'point' => 'number|egt:0',//给字段设置 >= 0
        'content' => 'require',//必填
        'state' => 'require|number|in:1,2,3,4,5,6,-1',//给字段设置范围
        'userid' => 'require',//必填
    ];

    /**
     * 提示消息
     */
    protected $message = [
        'point.number' => '积分必须是个数字',
        'point.egt' => '积分值必须是大于等于0',
        'content.require' => '消费描述必填',
        'state.require' => '状态必填',
        'state.number' => '状态必须是一个数字',
        'userid.require' => '用户信息未知',
    ];

    /**
     * 验证场景
     */
    protected $scene = [
    ];
}
?>