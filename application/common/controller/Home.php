<?php
namespace app\common\controller;

use think\Controller;

//公共前台控制器
class Home extends Controller
{
    public function __construct()
    {
        parent::__construct();

        //获取cookie 数据
        $LoginUser = cookie('LoginUser') ? cookie('LoginUser') : '';
        if(empty($LoginUser))
        {
            $this->error('请先登录',url('home/index/login'));
        }else{
            //获取用户表所有数据
            $UserList = model('User.User') -> column('id,salt');
            //halt($UserList);
            //存放id的字符串
            $UserId = '';
            foreach($UserList as $key => $value)
            {
                $UserSalt = md5($key.$value);
                if($LoginUser == $UserSalt)
                {
                    $UserId = $key;
                    break;
                }
            }
            //根据用户id查询用户数据 对象->数组 toArray
            $User = model('User.user')->with(['provinces'])->find($UserId);
            //halt($User);
            if(!$User)
            {
                cookie('LoginUser',null);
                $this->error('非法登录',url('home/index/login'));
            }
            //把用户数据给继承到home公共控制器的子控制器
            $this->LoginUser = $User;
            //赋值视图
            $this->assign([
                'LoginUser' => $User
            ]);
        }
    }
}




?>