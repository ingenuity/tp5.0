<?php

namespace app\api\controller\User;

use think\Controller;
use think\Request;

class AddRess extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->AddRessModel = model('User.AddRess');
    }

    public function Index()
    {
        // 接收用户id
        $userid = $this->request->param('userid');

        $AddRessList = $this->AddRessModel->with(['provinces','citys','districts'])->where(['userid' => $userid])->select();

        if(!$AddRessList)
        {
            $this->error('暂无收货地址');
        }

        $this->success('查询成功',null,$AddRessList);
    }

    // 修改收货地址
    public function select()
    {
        $params = $this->request->param();

        $user = $this->AddRessModel->where(['userid' => $params['userid']])->find();

        if(!$user)
        {
            $this->error('您不能修改他人地址');
        }

        $address = $this->AddRessModel->find($params['id']);

        if(!$address)
        {
            $this->error('该收货地址不存在');
        }

        // 这个用户的收货地址全部改为0再更新
        $status = $this->AddRessModel->where(['userid' => $params['userid']])->update(['status' => 0]);

        $data = [
            'id' => $params['id'],
            'status' => 1,
        ];

        $result = $this->AddRessModel->isUpdate(true)->save($data);

        if($result === FALSE || $status === FALSE)
        {
            $this->error('修改默认收货地址失败');
        }else{
            $this->success('修改默认地址成功');
        }
    }

    // 添加收货地址
    public function add()
    {
        $params = $this->request->param();

        $data = [
            'consignee' => $params['consignee'],
            'mobile' => $params['mobile'],
            'address' => $params['address'],
            'userid' => $params['userid'],
        ];

        // 设置默认
        if($params['status'] == 1)
        {
            $this->AddRessModel->where(['userid' => $params['userid']])->update(['status' => 0]);

            $data['status'] = $params['status'];
        }

        if(!empty($params['code']))
        {
            $parentpath = model('Region')->where(['code' => $params['code']])->value('parentpath');

            if(!$parentpath)
            {
                $this->error('所选地区不存在');
            }

            [$province,$city,$district] = explode(',',$parentpath);

            if($province)
            {
                $data['province'] = $province;
            }

            if($city)
            {
                $data['city'] = $city;
            }

            if($district)
            {
                $data['district'] = $district;
            }
        }
        // halt($data);
        $result = $this->AddRessModel->validate('common/User/AddRess')->save($data);
        // halt($result);
        if($result === FALSE)
        {
            $this->error($this->AddRessModel->getError());
        }else {
            $this->success('新增收货地址成功','/User/AddRess/Index');
        }
    }

    public function info()
    {
        $addrid = $this->request->param('addrid');

        $address = $this->AddRessModel->with(['provinces','citys','districts'])->find($addrid);

        // halt($address);

        if(!$address)
        {
            $this->error('该收货地址不存在');
        }
        $this->success('查询成功',null,$address);
    }

    public function order()
    {
        $userid = $this->request->param('userid');

        $address = model('User.AddRess')->where(['userid' => $userid,'status' => 1])->find();

        if(!$address)
        {
            $this->error('暂无默认收货地址');
        }

        $this->success('查询成功',null,$address);
    }

    
}
