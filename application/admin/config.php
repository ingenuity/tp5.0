<?php
//配置文件
return [
    'template' => [
        'layout_on' => true,
        'layout_name' => 'layout'
    ],

    // 默认跳转页面对应的模板文件
    'dispatch_success_tmpl'  => THINK_PATH . 'tpl' . DS . 'admin_jump.tpl',
    'dispatch_error_tmpl'    => THINK_PATH . 'tpl' . DS . 'admin_jump.tpl',
];