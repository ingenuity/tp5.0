<?php
namespace app\official\controller;

use mikkle\tp_wechat\base\BaseWechatApi;

class Index extends BaseWechatApi
{
    public function __construct()
    {
        parent::__construct();
    }

    protected $valid = true; //网站第一次匹配 true 1为匹配
    protected $isHook = false; //是否开启钩子

    public function auth()
    {
        // 调用父级的方法
        $this->index();

        $Subscribe = $this->returnEventSubscribe();

        $this->weObj->text($Subscribe)->reply();
    }

    // 默认关注回复信息
    protected function returnEventSubscribe()
    {
        $message = "欢迎来到K2215ask论坛\n使用帮助\n1、发送城市名称，可搜索天气\n2、发送预约单号，可搜索预约进度\n3、发送商品订单号，可搜索商品订单进度\n4、发送悬赏问题，可查看解决方案(提问：关键词)";

        return ['type' => 'text','message' => $message];
    }

    public function index()
    {
        return '<style type="text/css">*{ padding: 0; margin: 0; } div{ padding: 4px 48px;} a{color:#2E5CD5;cursor: pointer;text-decoration: none} a:hover{text-decoration:underline; } body{ background: #fff; font-family: "Century Gothic","Microsoft yahei"; color: #333;font-size:18px;} h1{ font-size: 100px; font-weight: normal; margin-bottom: 12px; } p{ line-height: 1.6em; font-size: 42px }</style><div style="padding: 24px 48px;"> <h1>:)</h1><p> ThinkPHP V5<br/><span style="font-size:30px">十年磨一剑 - 为API开发设计的高性能框架</span></p><span style="font-size:22px;">[ V5.0 版本由 <a href="http://www.qiniu.com" target="qiniu">七牛云</a> 独家赞助发布 ]</span></div><script type="text/javascript" src="http://tajs.qq.com/stats?sId=9347272" charset="UTF-8"></script><script type="text/javascript" src="http://ad.topthink.com/Public/static/client.js"></script><thinkad id="ad_bd568ce7058a1091"></thinkad>';
    }

}
