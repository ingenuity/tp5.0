<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Controller;
use think\Request;

class Admin extends Backend
{

    public function __construct()
    {
        parent::__construct();
        $this->UserModel = model("User.User");
    }

    // 管理员信息
    public function admin()
    {

        $AdminList = model('Admin')->select();

        return $this->fetch('', [
            'AdminList' => $AdminList
        ]);
    }

    // 新增管理员
    public function AdminAdd()
    {
        // 判断这是否post请求
        if ($this->request->isPost()) {
            // get post 接收全部参数
            $params = $this->request->param();
            if(!captcha_check($params['vercode']))
            {
                跳转到中间页
                $this->error('验证码错误');
            }

            $AdminAdd = model('Admin')->where('username',1)->find();
            
            // 判断是否已有该管理员名
            if($params['username'] == $AdminAdd['username'])
            {
                $this->error('该称呼已存在');
            }else if($params['nickname'] == $AdminAdd['nickname'])
            {
                $this->error('该昵称已存在');
            }
            

            // 判断密码和确认密码是否一致
            if ($params['pass'] != $params['repass']) {
                $this->error('密码与确认密码不一致');
            }

            // 生出密码盐
            $salt = build_ranstr();

            //进行加密
            $password = md5($params['pass'] . $salt);


            //封装插入数据库的数组
            $data = [
                'username' => $params['username'],
                'nickname' => $params['nickname'],
                'password' => $password,
                'salt' => $salt,
            ];
            // halt($data);
            $result = model('Admin')->validate('common/Admin/Admin')->save($data);

            if ($result === FALSE) {
                $this->error($this->UserModel->getError());
            } else {
                $this->success('新增成功', url('admin/admin/admin'), '', 1);
            }
        }
        //输出视图
        return $this->fetch();

        $AdminAddList = model('Admin')->select();

        return $this->fetch('', [
            'AdminAddList' => $AdminAddList
        ]);
    }

    // 修改管理员信息
    public function AdminFile()
    {
        $username = $this->request->param('username',0);
        $AdminFileList = model('Admin')->where(['username' => $username])->find();
        // halt($AdminFileList);
         // 判断这是否post请求
         if ($this->request->isPost()) {


            // get post 接收全部参数
            $params = $this->request->param();
            // halt($params);
            if(!captcha_check($params['vercode']))
            {
                跳转到中间页
                $this->error('验证码错误');
            }

            // 数据查询
            $Admins = model('Admin')->where('username',$params['username'] )->find();
                // halt($Admins);

            // 生出密码盐
            $salt = build_ranstr();

            // 加密处理
            $password = md5($params['password'].$Admins['salt']);
            $pass = md5($params['pass'].$Admins['salt']);
            $repass = md5($params['repass'].$Admins['salt']);

            // 判断密码是否一致
            if($password != $Admins['password'])
            {
                $this->error('原始密码输入错误');
            }else if( $password == $pass) {
                $this->error('原始密码与新密码一致');
            }else if ($pass != $repass) {
                $this->error('密码与确认密码不一致');
            }

            //封装插入数据库的数组
            $data = [
                'id' => $AdminFileList['id'],
                'username' => $params['username'],
                'nickname' => $params['nickname'],
                'password' => $repass,
                'salt' => $salt
            ];
            // halt($data);
            // validate：验证器
            $result = model('Admin')->validate('common/Admin/Admin')->isUpdate(true)->save($data);
            // halt($result);
            if ($result === FALSE) {
                $this->error(model('Admin')->getError());
            } else {
                $this->success('更改成功', url('admin/admin/admin'), '', 1);
            }
        }

        

        return $this->fetch('', [
            'AdminFileList' => $AdminFileList
        ]);
    }

    // 用户信息
    public function user()
    {

        $UserList = model('User.User')->select();

        return $this->fetch('', [
            'UserList' => $UserList
        ]);
    }

    // 修改用户信息
    public function Userfile()
    {
        $id = $this->request->param('id');


        // 查询用户信息
        $Userfile = model('User.User')->with(['provinces', 'citys', 'districts'])->where(['id' => $id])->find();

        // halt($Userfile);
        // 查询地区的省份
        $provinces = model('Region')->where(['grade' => 1])->select();
        //  $citys = model('Region')->where(['grade' => 2])->select();
        //  $districts = model('Region')->where(['grade' => 3])->select();

        // 查询市
        if ($Userfile['province']) {
            $citys = model('Region')->where(['parentid' => $Userfile['province']])->select();
        } else {
            $citys = [];
        }
        // halt($citys);

        // 查询区
        if ($Userfile['city']) {
            $districts = model('Region')->where(['parentid' => $Userfile['city']])->select();
        } else {
            $districts = [];
        }




        return $this->fetch('', [
            'Userfile' => $Userfile,
            'provinces' => $provinces,
            'citys' => $citys,
            'districts' => $districts
        ]);
    }

    // 新增用户信息
    public function UserAdd()
    {

        $UserList = model('User.User')->select();

        return $this->fetch('', [
            'UserList' => $UserList
        ]);
    }

    //删除信息
    public function del()
    {
        if ($this->request->isAjax()) {
            $userid = $this->request->param('userid');
            $user = $this->UserModel->find($userid);
            // halt($user);
            if (!$user) {
                $this->error('该信息不存在，无法删除');
            }

            $result = $this->UserModel->destroy($userid);
            // halt($result);
            if ($result === FALSE) {
                $this->error('删除失败');
            }
            $this->success('删除成功', url('admin/admin/user'));
        }
    }

    // 删除管理员
    public function AdminDel()
    {
        if ($this->request->isAjax()) {
            $AdminListid = $this->request->param('id');
            $user = model('admin')->find($AdminListid);
            // halt($AdminListid);
            if (!$user) {
                $this->error('该信息不存在，无法删除');
            }

            $result = model('admin')->destroy($AdminListid);
            // halt($result);
            if ($result === FALSE) {
                $this->error('删除失败');
            }
            $this->success('删除成功', url('admin/admin/admin'));
        }
    }
}
