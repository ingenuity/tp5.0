<?php

namespace app\home\controller;

use app\common\controller\Home;

use PHPMailer\PHPMailer\PHPMailer;

use think\Controller;
use think\Request;

class User extends Home
{
    //构造函数
    public function __construct()
    {
        parent::__construct();
        $this->UserModel = model('User.User');
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {


        $userid = $this->request->param('userid',0);

        if($userid)
        {
            $User = $this->UserModel->find($userid);
            if(!$User)
            {
                $this->error('该用户不存在');
            }

            $Postlist = model('Post.Post')->where(['userid'=> $userid])->select();

            $PostId = model('Post.Comment')->where(['userid' => $userid])->select();

             // 输出视图
            return $this->fetch('user',[
                'User' => $User,
                'Postlist' => $Postlist,
                'PostId' => $PostId
            ]);
        }

        $LoginPostlist = model('Post.Post')->where('userid' , $this->LoginUser['id'])->select();

        $LoginPostId = model('Post.Comment')->where(['userid' => $userid])->select();


        // 输出视图
        return $this->fetch('',[
            'LoginPostlist' => $LoginPostlist,
            'LoginPostId' => $LoginPostId
        ]);

         // 输出视图
         return $this->fetch();
    }

    /**
     * 显示消息列表
     *
     * @return \think\Response
     */
    public function message()
    {
        $NavTitle = '消息列表';
        return $this->fetch("", [
            'navTitle' => $NavTitle
        ]);
    }

    /**
     * 显示设置资料
     *
     * @return \think\Response
     */
   // 基本资料
   public function profile()
   {
       $navTitle = '基本资料';

       if($this->request->isPost())
       {
           $action = $this->request->param('action','');

           if($action == 'profile')
           {
               $params = $this->request->param();

               // 封装数据
               $data = [
                   'id' => $this->LoginUser['id'],
                   'nickname' => $params['nickname'],
                   'sex' => $params['sex'],
                   'province' => $params['province'],
                   'city' => $params['city'],
                   'district' => $params['district'],
                   'content' => $params['content']
               ];

               // isUpdate == true ?  save = 更新数据  : 插入
               $result = $this->UserModel->isUpdate(true)->save($data);

               if($result === FALSE)
               {
                   $this->error('更新基本资料失败');
               }else{
                   $this->success('更新基本资料成功',url('home/user/index'));
               }
           }

           if($action == 'avatar')
           {
               $avatar = upload('avatar');

               if($avatar['code'] === 0)
               {
                   $this->error($avatar['msg']);
               }

               $data = [
                   'id' => $this->LoginUser['id'],
                   'avatar' => $avatar['data']
               ];

               $result = $this->UserModel->isUpdate(true)->save($data);

               if($result === FALSE)
               {
                   @is_file('.' . $data['avatar']) && unlink('.' . $data['avatar']);
                   $this->error('更新头像失败');
               }else{
                   @is_file('.' . $this->LoginUser['avatar']) && unlink('.' . $this->LoginUser['avatar']);
                   $this->success('更新头像成功');
               }
           }

           if($action == 'cover')
           {
               $cover = upload('cover');

               if($cover['code'] === 0)
               {
                   $this->error($cover['msg']);
               }

               $data = [
                   'id' => $this->LoginUser['id'],
                   'cover' => $cover['data']
               ];

               $result = $this->UserModel->isUpdate(true)->save($data);

               if($result === FALSE)
               {
                   @is_file('.' . $data['cover']) && unlink('.' . $data['cover']);
                   $this->error('更新背景失败');
               }else{
                   @is_file('.' . $this->LoginUser['cover']) && unlink('.' . $this->LoginUser['cover']);
                   $this->success('更新背景成功');
               }
           }
       }


       // 查询地区的省份
       $provinces = model('Region')->where(['grade' => 1])->select();
       
       // 查询市
       if($this->LoginUser['province'])
       {
           $citys = model('Region')->where(['parentid' => $this->LoginUser['province']])->select();
       }else{
           $citys = [];
       }

       // 查询区
       if($this->LoginUser['city'])
       {
           $districts = model('Region')->where(['parentid' => $this->LoginUser['city']])->select();
       }else{
           $districts = [];
       }

       return $this->fetch('',[
           'navTitle' => $navTitle,
           'provinces' => $provinces,
           'citys' => $citys,
           'districts' => $districts
       ]);
   }
    //查询地区
    public function area()
    {
        if ($this->request->isAjax()) {
            $code = $this->request->param('code', '');
            $list = model('Region')->where(['parentid' => $code])->select();
            if (!$list) {
                $this->error('选择地区不存在');
            }
            $this->success('查询成功', null, $list);
        }
    }

    /**
     * 显示用户中心
     *
     * @return \think\Response
     */
    public function home()
    {
        $NavTitle = '用户中心';
        
        return $this->fetch('', [
            'navTitle' => $NavTitle,
        ]);
    }

    /**
     * 积分消费中心
     */
    public function record()
    {
        $NavTitle = '积分消费中心';

       $RecordList = model('User.Record')->order('createtime','desc')->where(['userid' => $this->LoginUser['id']])->paginate(9);
        return $this->fetch('', [
            'navTitle' => $NavTitle,
            'RecordList' => $RecordList,
        ]);
        
    }

    /**
     * 发送邮件
     */
    public function email()
    {
        if ($this->request->isAjax()) {
            $email = $this->request->param('email', '');
            //返回josn格式数据
            //return json($email);
            /* 
                判断该邮箱是否已注册了

                生成随机字符串

                设置session

                再随机字符串发送该邮箱

                手动输入字符串

                在注册功能 获取session那个字符串以及接收表单字符串 => 作判断
            */


            $User = $this->UserModel->where(['email' => $email])->find();
        }
        if (!$User) {
            $this->error('该邮箱不存在');
        }
        $Email = md5($User['email'] . $User['salt']);
        $content = "<a href='http://www.k2215.com/home/index/auth?email=$Email'>点击认证</a>";

        $mail = new PHPMailer(true);

        //开启SMTP
        $mail->isSMTP(true);

        /**
         * SMTP服务器
         * 网易邮箱：smtp.163.com
         * QQ邮箱: qq => smtp.qq.com
         */
        $mail->Host = "smtp.qq.com";

        //开启认证
        $mail->SMTPAuth = true;

        //发送内容支持html
        $mail->IsHTML(true);

        //账号
        $mail->Username = 'uytrewqqwerty@qq.com';

        //密码 => 不是邮箱密码，开启SMTP时的授权码
        $mail->Password = 'qsonwqzducsnebhc';

        //端口 服务器需要465端口
        $mail->Port = 465;

        //协议
        $mail->SMTPSecure = 'ssl';

        //发送邮件
        $mail->From = 'uytrewqqwerty@qq.com';

        //发件人名字
        $mail->FromName = '创领论坛';

        //接收回复的邮箱
        $mail->addReplyTo('uytrewqqwerty@qq.com', "创领论坛");

        //邮箱主题
        $mail->addAddress($User['email'], $User['nickname']);

        //邮箱主题
        $mail->Subject = '账号认证';

        //设置多少个字符换行 80字符
        $mail->WordWrap = 80;

        // 发送HTML内容
        $mail->msgHTML($content);

        $result = $mail->send();

        if ($result === FALSE) {
            $this->error('发送邮件失败');
        } else {
            $this->success('发送邮件成功');
        }
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
