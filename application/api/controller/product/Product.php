<?php

namespace app\api\controller\Product;

use think\Controller;

class Product extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->TypeModel = model('Product.Type');
        $this->ProductModel = model('Product.Product');
        $this->VipModel = model('Product.Vip');
    }

    public function index()
    {
        $typeid = $this->request->param('typeid',0);

        $flag = $this->request->param('flag',0);

        $OrderBy = $this->request->param('OrderBy','id');

        $where['typeid'] = $typeid;
        

        if($flag != 0)
        {
            $where['flag'] = $flag;
        }


        $list = $this->ProductModel->where($where)->order($OrderBy,'desc')->select();

        

        $this->success('查询成功',null,$list);

    }

    public function type()
    {
        $type = $this->TypeModel->order('id','asc')->select();

        if(!$type)
        {
            $this->error('暂无数据');
        }

        $this->success('查询成功',null,$type);
    }

    public function info()
    {
        $proid = $this->request->param('proid');

        $product = $this->ProductModel->find($proid);

        if(!$product)
        {
            $this->error('商品不存在');
        }

        $this->success('查询成功',null,$product);
    }

    // 分类页面
    public function onTvi()
    {
        $product = $this->TypeModel->select();
        // halt($product);
        if(!$product)
        {
            $this->error('商品不存在');
        }

        $this->success('查询成功',null,$product);
    }

    // 分类页面内容
    public function PoreList()
    {
        $proid = $this->request->param('proid');

        $product = $this->ProductModel->find($proid);

        if(!$product)
        {
            $this->error('商品不存在');
        }

        $this->success('查询成功',null,$product);
    }
}
