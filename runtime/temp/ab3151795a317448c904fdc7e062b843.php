<?php if (!defined('THINK_PATH')) exit(); /*a:6:{s:79:"F:\!Abeifen\WWW\Thinkphp__HTGL\public/../application/home\view\index\index.html";i:1653892884;s:64:"F:\!Abeifen\WWW\Thinkphp__HTGL\application\home\view\layout.html";i:1653476814;s:69:"F:\!Abeifen\WWW\Thinkphp__HTGL\application\home\view\common\meta.html";i:1653471571;s:71:"F:\!Abeifen\WWW\Thinkphp__HTGL\application\home\view\common\script.html";i:1652864157;s:71:"F:\!Abeifen\WWW\Thinkphp__HTGL\application\home\view\common\header.html";i:1654352686;s:71:"F:\!Abeifen\WWW\Thinkphp__HTGL\application\home\view\common\footer.html";i:1652755560;}*/ ?>
<!-- 主页面 -->
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- 引用样式 -->
    <!--  -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>创领论坛</title>
<link rel="stylesheet" href="/static/home/res/layui/css/layui.css">
<link rel="stylesheet" href="/static/home/res/css/global.css">
<link rel="stylesheet" href="/static/home/res/css/common.css">
    <!-- 引用js  -->
    <script src="/static/home/res/layui/layui.js"></script>
<script>
layui.cache.page = '';
layui.cache.user = {
  username: '游客'
  ,uid: -1
  ,avatar: '/static/home/res/images/avatar/00.jpg'
  ,experience: 83
  ,sex: '男'
};
layui.config({
  version: "3.0.0"
  ,base: '/static/home/res/mods/' //这里实际使用时，建议改成绝对路径
}).extend({
  fly: 'index'
}).use('fly');
</script>

</head>
<body>
    <!-- 引用头部 -->
    <!-- 头部公共样式 -->
<div class="fly-header layui-bg-black">
    <div class="layui-container">
        <a class="fly-logo" href="<?php echo url('home/index/index'); ?>">
            <img src="/static/home/res/images/logo.png" alt="layui" />
        </a>

        <ul class="layui-nav fly-nav-user">
            <!-- 未登入的状态 -->
            <!-- <li class="layui-nav-item">
          <a class="iconfont icon-touxiang layui-hide-xs" href="user/login.html"></a>
        </li>
        <li class="layui-nav-item">
          <a href="user/login.html">登入</a>
        </li>
        <li class="layui-nav-item">
          <a href="user/reg.html">注册</a>
        </li>
        <li class="layui-nav-item layui-hide-xs">
          <a href="/app/qq/" onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})" title="QQ登入" class="iconfont icon-qq"></a>
        </li>
        <li class="layui-nav-item layui-hide-xs">
          <a href="/app/weibo/" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})" title="微博登入" class="iconfont icon-weibo"></a>
        </li> -->

            <!-- 登入后的状态 -->
            <?php if(\think\Request::instance()->action() != 'login' && \think\Request::instance()->action() !='register'): if($LoginUser): ?>
            <li class="layui-nav-item">
              <a href="<?php echo url('admin/index/index'); ?>">后台</a>
            </li>
            <li class="layui-nav-item">
                <a class="fly-nav-avatar" href="javascript:;">
                    <cite class="layui-hide-xs"><?php echo $LoginUser['nickname']; ?></cite>
                    <i class="iconfont icon-renzheng layui-hide-xs" title="认证信息：<?php echo $LoginUser['nickname']; ?>"></i>
                    <i class="layui-badge fly-badge-vip layui-hide-xs">VIP <?php echo $LoginUser['vip']; ?></i>
                    <img src="<?php echo $LoginUser['avatar_cdn']; ?>" alt="<?php echo $LoginUser['nickname']; ?>">
                </a>
                <dl class="layui-nav-child">
                    <dd>
                        <a href="<?php echo url('home/user/profile'); ?>"><i class="layui-icon">&#xe620;</i>基本资料</a>
                    </dd>
                    <dd>
                        <a href="<?php echo url('home/user/message'); ?>"><i class="iconfont icon-tongzhi" style="top: 4px"></i>我的消息</a>
                    </dd>
                    <dd>
                        <a href="<?php echo url('home/user/index'); ?>"><i class="layui-icon"
                                style="margin-left: 2px; font-size: 22px">&#xe68e;</i>我的主页</a>
                    </dd>
                    <hr style="margin: 5px 0" />
                    <dd><a href="<?php echo url('home/index/logout'); ?>" style="text-align: center">退出</a></dd>
                </dl>
            </li>
            <?php else: ?>
            <!-- 未登入的状态 -->
            <li class="layui-nav-item">
                <a class="iconfont icon-touxiang layui-hide-xs" href="<?php echo url('home/user/login'); ?>"></a>
            </li>
            <li class="layui-nav-item">
              <a href="<?php echo url('admin/index/index'); ?>">后台</a>
            </li>
            <li class="layui-nav-item">
                <a href="<?php echo url('home/index/login'); ?>">登入</a>
            </li>
            <li class="layui-nav-item">
                <a href="<?php echo url('home/index/register'); ?>">注册</a>
            </li>
            <!-- <li class="layui-nav-item layui-hide-xs">
        <a
          href="/app/qq/"
          onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})"
          title="QQ登入"
          class="iconfont icon-qq"
        ></a>
      </li>
      <li class="layui-nav-item layui-hide-xs">
        <a
          href="/app/weibo/"
          onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})"
          title="微博登入"
          class="iconfont icon-weibo">
        </a>
      </li> -->
            <?php endif; endif; ?>
        </ul>
    </div>
</div>
    
    <!-- 主页内容 -->
<div class="fly-panel fly-column">
    <div class="layui-container">
        <ul class="layui-clear">
            <li class="layui-hide-xs <?php echo $cateid==''?'layui-this' : ''; ?>"><a href="<?php echo url('/'); ?>">首页</a></li>
            <?php foreach($CateList as $item): ?>
            <li class="<?php echo $cateid==$item['id']?'layui-this' : ''; ?>"><a href="<?php echo url('home/index/index',['cateid' => $item['id'],'accept' => $accept,'state' => $state]); ?>"><?php echo $item['name']; ?></a></li>
            <?php endforeach; ?>
            <li class="layui-hide-xs layui-hide-sm layui-show-md-inline-block"><span class="fly-mid"></span></li>

            <!-- 用户登入后显示 -->
            <?php if($LoginUser): ?>
            <li class="layui-hide-xs layui-hide-sm layui-show-md-inline-block"><a href="user/index.html">我发表的贴</a></li>
            <li class="layui-hide-xs layui-hide-sm layui-show-md-inline-block"><a
                    href="user/index.html#collection">我收藏的贴</a></li>
            <?php endif; ?>
        </ul>

        <div class="fly-column-right layui-hide-xs">
            <span class="fly-search"><i class="layui-icon"></i></span>
            <a href="<?php echo url('home/post/add'); ?>" class="layui-btn">发表新帖</a>
        </div>
        <div class="layui-hide-sm layui-show-xs-block"
            style="margin-top: -10px; padding-bottom: 10px; text-align: center;">
            <a href="<?php echo url('home/post/add'); ?>" class="layui-btn">发表新帖</a>
        </div>
    </div>
</div>

<div class="layui-container">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md8">

            <div class="fly-panel" style="margin-bottom: 0;">

                <div class="fly-panel-title fly-filter">
                    <a href="<?php echo url('home/index/index',['state' => '','cateid' => $cateid,'accept' => $accept]); ?>" class="<?php echo $state==''?'layui-this':''; ?>">全部</a>
                    <span class="fly-mid"></span>
                    <a href="<?php echo url('home/index/index',['state' => 1,'cateid' => $cateid,'accept' => $accept]); ?>" class="<?php echo $state==1?'layui-this':''; ?>">置顶</a>
                    <span class="fly-mid"></span>
                    <a href="<?php echo url('home/index/index',['state' => 2,'cateid' => $cateid,'accept' => $accept]); ?>" class="<?php echo $state==2?'layui-this':''; ?>">精华</a>
                    <span class="fly-mid"></span>
                    <a href="<?php echo url('home/index/index',['state' => 3,'cateid' => $cateid,'accept' => $accept]); ?>" class="<?php echo $state==3?'layui-this':''; ?>">热门</a>
                    <span class="fly-filter-right layui-hide-xs">
                        <a href="<?php echo url('home/index/index',['accept' => 1,'state' => $state,'cateid' => $cateid]); ?>" class="<?php echo $accept==1?'layui-this' : ''; ?>">已采纳</a>
                        <span class="fly-mid"></span>
                        <a href="<?php echo url('home/index/index',['accept' => 0,'state' => $state,'cateid' => $cateid]); ?>" class="<?php echo $accept==0?'layui-this' : ''; ?>">未采纳</a>
                    </span>
                </div>

                <ul class="fly-list">
                    <?php foreach($PostList as $item): ?>
                    <li>
                        <a href="<?php echo url('home/user/index',['userid' => $item['user']['id']]); ?>" class="fly-avatar">
                            <img src="<?php echo $item['user']['avatar_cdn']; ?>"
                                alt="<?php echo $item['user']['nickname']; ?>">
                        </a>
                        <h2>
                            <a class="layui-badge"><?php echo $item['cate']['name']; ?></a>
                            <a href="<?php echo url('home/index/info',['postid' => $item['id']]); ?>"><?php echo $item['title']; ?></a>
                        </h2>
                        <div class="fly-list-info">
                            <a href="<?php echo url('home/user/index',['userid' => $item['user']['id']]); ?>" link>
                                <cite><?php echo $item['user']['nickname']; ?></cite>
                                <?php if($item['user']['auth'] == 1): ?>
                                    <i class="iconfont icon-renzheng" title="认证信息：<?php echo $item['user']['nickname']; ?>"></i>
                                <?php endif; ?>
                                <i class="layui-badge fly-badge-vip">VIP <?php echo $item['user']['vip']; ?></i>
                            </a>
                            <span><?php echo $item['createtime']; ?></span>

                            <span class="fly-list-kiss layui-hide-xs" title="悬赏飞吻"><i class="iconfont icon-kiss"></i>
                                <?php echo $item['point']; ?></span>
                            <?php if($item['accept']): ?>
                            <span class="layui-badge fly-badge-accept layui-hide-xs">已结</span>
                            <?php endif; ?>
                            <span class="fly-list-nums">
                                <i class="iconfont icon-pinglun1" title="回答"></i> <?php echo $item['comment_count']; ?>
                            </span>
                        </div>
                        <div class="fly-list-badge">
                            <?php if($item['state'] == 1): ?>
                                <span class="layui-badge layui-bg-red">置顶</span>
                            <?php elseif($item['state'] == 2): ?>
                                <span class="layui-badge layui-bg-red">精华</span>
                            <?php elseif($item['state'] == 3): ?>
                                <span class="layui-badge layui-bg-red">热门</span>
                            <?php endif; ?>
                        </div>
                    </li>
                        <?php endforeach; ?>

                </ul>
                <!-- <div style="text-align: center">
            <div class="laypage-main">
              <a href="jie/index.html" class="laypage-next">更多求解</a>
            </div>
          </div> -->
                <?php echo $PostList->render(); ?>
            </div>
        </div>
        <div class="layui-col-md4">

            <div class="fly-panel fly-signin">
                <div class="fly-panel-title">
                    签到

                    <!-- <span class="fly-signin-days">已连续签到<cite>16</cite>天</span> -->
                </div>
                <div class="fly-panel-main fly-signin-main">

                    <?php if(isset($LoginUser['sing'])): ?>
                      
                        <button class="layui-btn layui-btn-disabled">今日已签到</button>
                        <span>获得了<cite><?php echo $SignPoint; ?></cite>积分</span>
                    <?php else: ?>
                        <button class="layui-btn layui-btn-danger" id="LAY_signin">今日签到</button>
                        <span>可获得<cite><?php echo $SignPoint; ?></cite>积分</span>
                    <?php endif; ?>
                    <!-- 已签到状态 -->
                    
                </div>
            </div>

            <div class="fly-panel">
                <h3 class="fly-panel-title">本周热议</h3>
                <ul class="fly-panel-main fly-list-static">
                    <?php foreach($HotList as $item): ?>
                        <li>
                            <a href="<?php echo url('home/index/info',['postid' => $item['postid']]); ?>" target="_blank"><?php echo $item['post']['title']; ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <div class="fly-panel fly-rank fly-rank-reply" id="LAY_replyRank">
                <h3 class="fly-panel-title">回贴周榜</h3>
                <dl>
                    <!--<i class="layui-icon fly-loading">&#xe63d;</i>-->
                    <dd>
                        <a href="user/home.html">
                            <img
                                src="https://tva1.sinaimg.cn/crop.0.0.118.118.180/5db11ff4gw1e77d3nqrv8j203b03cweg.jpg"><cite>贤心</cite><i>106次回答</i>
                        </a>
                    </dd>
                   
                </dl>
            </div>

            <div class="fly-panel" style="padding: 20px 0; text-align: center;">
                <img src="/static/home/res/images/weixin.jpg" style="max-width: 100%;" alt="layui">
                <p style="position: relative; color: #666;">微信扫码关注 layui 公众号</p>
            </div>
        </div>
    </div>
</div>

<script>
    layui.use(['layer'],function()
    {
        var $ = layui.jquery,
        layer = layui.layer

        $('#LAY_signin').click(function(){
            $.ajax({
                type:'post',
                url:`<?php echo url('home/index/sign'); ?>`,
                dataType:"json",
                success:function(res)
                {
                    if(res.code === 1)
                    {
                        layer.msg(res.msg,function(){
                            location.reload()
                        })
                    }else{
                        layer.msg(res.msg)
                    }
                }
            })
        })
    })
</script>
    

    <!-- 引用底部 -->
    <div class="fly-footer">
    <p>2017 &copy; <a href="http://www.layui.com/" target="_blank">Fly技术社区</a></p>
</div>
</body>
</html>