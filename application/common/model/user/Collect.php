<?php

namespace app\common\model\User;

use think\Model;

class Collect extends Model
{
    //收藏表
    protected $table = 'pre_collect';

    // 自动写入时间
    protected $autoWriteTimestamp = true;

    // 创建时间 => 注册时间
    protected $createTime = 'createtime';

    // 更新时间
    protected $updateTime = false;
}
