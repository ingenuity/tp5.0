<?php if (!defined('THINK_PATH')) exit(); /*a:6:{s:70:"F:\!Abeifen\WWW\tp5.0\public/../application/admin\view\post\index.html";i:1654741400;s:56:"F:\!Abeifen\WWW\tp5.0\application\admin\view\layout.html";i:1653877040;s:61:"F:\!Abeifen\WWW\tp5.0\application\admin\view\common\meta.html";i:1653968148;s:63:"F:\!Abeifen\WWW\tp5.0\application\admin\view\common\script.html";i:1653894646;s:61:"F:\!Abeifen\WWW\tp5.0\application\admin\view\common\menu.html";i:1654351541;s:63:"F:\!Abeifen\WWW\tp5.0\application\admin\view\common\header.html";i:1654353018;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="keywords" content="LightYear,LightYearAdmin,光年,后台模板,后台管理系统,光年HTML模板">
<meta name="description" content="Light Year Admin V4是一个基于Bootstrap v4.4.1的后台管理系统的HTML模板。">
<meta name="author" content="yinq">
<title><?php echo $system['SiteName']; ?>后台管理系统</title>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo $system['favicon']; ?>">
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<link rel="stylesheet" type="text/css" href="/static/admin/css/materialdesignicons.min.css">
<link rel="stylesheet" type="text/css" href="/static/admin/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/static/admin/css/animate.min.css">
<link rel="stylesheet" type="text/css" href="/static/admin/css/style.min.css">

    <script type="text/javascript" src="/static/admin/js/jquery.min.js"></script>
<script type="text/javascript" src="/static/admin/js/popper.min.js"></script>
<script type="text/javascript" src="/static/admin/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/static/admin/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/static/admin/js/jquery.cookie.min.js"></script>
<script type="text/javascript" src="/static/admin/js/main.min.js"></script>
<script type="text/javascript" src="/static/admin/js/Chart.min.js"></script>
</head>
<body>
    <div class="lyear-layout-web">
        <div class="lyear-layout-container">

            <!-- 引用侧边菜单 -->
            <!--左侧导航-->
<aside class="lyear-layout-sidebar">

  <!-- logo -->
  <div id="logo" class="sidebar-header">
    <a href="<?php echo url('admin/index/index'); ?>"><img src="<?php echo $system['Logo']; ?>" title="LightYear" alt="LightYear" /></a>
  </div>
  <div class="lyear-layout-sidebar-info lyear-scroll">

    <nav class="sidebar-main">
      <ul class="nav-drawer">

        <!-- 后台首页 -->
        <li class="nav-item active"> <a href="<?php echo url('admin/index/index'); ?>"><i class="mdi mdi-home"></i>
            <span>后台首页</span></a> </li>

        <!-- 网络配置 -->
        <li class="nav-item"> <a href="<?php echo url('admin/config/index'); ?>"><i class="mdi mdi-tools"></i> <span>网站配置</span></a>
        </li>

        <!-- 信息管理 -->
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-account-multiple-outline"></i> <span>信息管理</span></a>
          <ul class="nav nav-subnav">
            <li> <a href="<?php echo url('admin/admin/user'); ?>"><i class="mdi mdi-account-alert-outline"></i>
                <span>用户信息管理</span></a> </li>
            <li> <a href="<?php echo url('admin/admin/admin'); ?>"><i class="mdi mdi-account-alert"></i> <span>管理员信息管理</span></a>
            </li>
          </ul>
        </li>

        <!-- 帖子管理 -->
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-account-multiple-outline"></i> <span>帖子管理</span></a>
          <ul class="nav nav-subnav">
            <li> <a href="<?php echo url('admin/post/index'); ?>"><i class="mdi mdi-account-alert-outline"></i><span>帖子信息管理</span></a> </li>
          </ul>
        </li>

        <!-- 元素配置 -->
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-palette"></i> <span>数据统计</span></a>
          <ul class="nav nav-subnav">
            <li> <a href="<?php echo url('admin/analysis/user'); ?>"><span>用户统计</span></a> </li>
          </ul>
        </li>

        <!-- 表单 -->
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-format-align-justify"></i> <span>表单</span></a>
          <ul class="nav nav-subnav">
            <li> <a href="lyear_forms_elements.html">基本元素</a> </li>
            <li> <a href="lyear_forms_input_group.html">输入框组</a> </li>
            <li> <a href="lyear_forms_radio.html">单选框</a> </li>
            <li> <a href="lyear_forms_checkbox.html">复选框</a> </li>
            <li> <a href="lyear_forms_switches.html">开关</a> </li>
            <li> <a href="lyear_forms_range.html">范围选择</a> </li>
          </ul>
        </li>

        <!-- 工具类 -->
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-tools"></i> <span>工具类</span></a>
          <ul class="nav nav-subnav">
            <li> <a href="lyear_utilities_borders.html">边框</a> </li>
            <li> <a href="lyear_utilities_colors.html">颜色</a> </li>
            <li> <a href="lyear_utilities_display.html">显示属性</a> </li>
            <li> <a href="lyear_utilities_flex.html">弹性布局</a> </li>
            <li> <a href="lyear_utilities_float.html">浮动</a> </li>
            <li> <a href="lyear_utilities_sizing.html">尺寸</a> </li>
            <li> <a href="lyear_utilities_spacing.html">间隔</a> </li>
            <li> <a href="lyear_utilities_stretched_link.html">延伸链接</a> </li>
            <li> <a href="lyear_utilities_text.html">文本</a> </li>
            <li> <a href="lyear_utilities_other.html">其他</a> </li>
          </ul>
        </li>

        <!-- 示例页面 -->
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-file-outline"></i> <span>示例页面</span></a>
          <ul class="nav nav-subnav">
            <li> <a href="lyear_pages_doc.html">文档列表</a> </li>
            <li> <a href="lyear_pages_gallery.html">图库列表</a> </li>
            <li> <a href="lyear_pages_config.html">网站配置</a> </li>
            <li> <a href="lyear_pages_rabc.html">设置权限</a> </li>
            <li> <a href="lyear_pages_add_doc.html">新增文档</a> </li>
            <li> <a href="lyear_pages_guide.html">表单向导</a> </li>
            <li> <a href="lyear_pages_data_table.html">表格插件</a> </li>
            <li> <a href="lyear_pages_login_1.html" target="_blank">登录页面1</a> </li>
            <li> <a href="lyear_pages_login_2.html" target="_blank">登录页面2</a> </li>
            <li> <a href="lyear_pages_login_3.html" target="_blank">登录页面3</a> </li>
            <li> <a href="lyear_pages_login_4.html" target="_blank">登录页面4</a> </li>
            <li> <a href="lyear_pages_error.html" target="_blank">错误页面</a> </li>
          </ul>
        </li>

        <!-- js插件 -->
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-language-javascript"></i> <span>JS 插件</span></a>
          <ul class="nav nav-subnav">
            <li> <a href="lyear_js_datepicker.html">日期选取器</a> </li>
            <li> <a href="lyear_js_colorpicker.html">选色器</a> </li>
            <li> <a href="lyear_js_chartjs.html">Chart.js</a> </li>
            <li> <a href="lyear_js_jconfirm.html">对话框</a> </li>
            <li> <a href="lyear_js_tags_input.html">标签插件</a> </li>
            <li> <a href="lyear_js_notify.html">通知消息</a> </li>
            <li> <a href="lyear_js_maxlength.html">长度判断</a> </li>
            <li> <a href="lyear_js_select.html">下拉选择</a> </li>
            <li> <a href="lyear_js_fullcalendar.html">日程插件</a> </li>
            <li> <a href="lyear_js_loading.html">loading插件</a> </li>
          </ul>
        </li>

        <!-- 多级菜单 -->
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-menu"></i> <span>多级菜单</span></a>
          <ul class="nav nav-subnav">
            <li> <a href="#!">一级菜单</a> </li>
            <li class="nav-item nav-item-has-subnav">
              <a href="#!">一级菜单</a>
              <ul class="nav nav-subnav">
                <li> <a href="#!">二级菜单</a> </li>
                <li class="nav-item nav-item-has-subnav">
                  <a href="#!">二级菜单</a>
                  <ul class="nav nav-subnav">
                    <li> <a href="#!">三级菜单</a> </li>
                    <li> <a href="#!">三级菜单</a> </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li> <a href="#!">一级菜单</a> </li>
          </ul>
        </li>
      </ul>
    </nav>

    <div class="sidebar-footer">
      <p class="copyright">Copyright &copy; 2019. <a target="_blank" href="http://lyear.itshubao.com">IT书包</a> All
        rights reserved.</p>
    </div>
  </div>

</aside>
<!--End 左侧导航-->

            <!-- 引用头部 -->
            <!--头部信息-->
<header class="lyear-layout-header">
      
    <nav class="navbar">
    
      <div class="navbar-left">
        <div class="lyear-aside-toggler">
          <span class="lyear-toggler-bar"></span>
          <span class="lyear-toggler-bar"></span>
          <span class="lyear-toggler-bar"></span>
        </div>
      </div>
      
      <ul class="navbar-right d-flex align-items-center">
        <li class="dropdown dropdown-notice">
          <span data-toggle="dropdown" class="icon-item">
            <i class="mdi mdi-bell-outline"></i>
            <span class="badge badge-danger">10</span>
          </span>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="lyear-notifications">
              
              <div class="lyear-notifications-title clearfix"  data-stopPropagation="true"><a href="#!" class="float-right">查看全部</a>你有 10 条未读消息</div>
              <div class="lyear-notifications-info lyear-scroll">
                <a href="#!" class="dropdown-item" title="树莓派销量猛增，疫情期间居家工作学习、医疗领域都需要它">树莓派销量猛增，疫情期间居家工作学习、医疗领域都需要它</a>
                <a href="#!" class="dropdown-item" title="GNOME 用户体验团队将为 GNOME Shell 提供更多改进">GNOME 用户体验团队将为 GNOME Shell 提供更多改进</a>
                <a href="#!" class="dropdown-item" title="Linux On iPhone 即将面世，支持 iOS 的双启动">Linux On iPhone 即将面世，支持 iOS 的双启动</a>
                <a href="#!" class="dropdown-item" title="GitHub 私有仓库完全免费面向团队提供">GitHub 私有仓库完全免费面向团队提供</a>
                <a href="#!" class="dropdown-item" title="Wasmtime 为 WebAssembly 增加 Go 语言绑定">Wasmtime 为 WebAssembly 增加 Go 语言绑定</a>
                <a href="#!" class="dropdown-item" title="红帽借“订阅”成开源一哥，首创者 Cormier 升任总裁">红帽借“订阅”成开源一哥，首创者 Cormier 升任总裁</a>
                <a href="#!" class="dropdown-item" title="Zend 宣布推出两项 PHP 新产品">Zend 宣布推出两项 PHP 新产品</a>
              </div>
              
            </div>
          </div>
        </li>
        <!--切换主题配色-->
        <li class="dropdown dropdown-skin">
          <span data-toggle="dropdown" class="icon-item"><i class="mdi mdi-palette"></i></span>
          <ul class="dropdown-menu dropdown-menu-right" data-stopPropagation="true">
            <li class="drop-title"><p>主题</p></li>
            <li class="drop-skin-li clearfix">
              <span class="inverse">
                <input type="radio" name="site_theme" value="default" id="site_theme_1" checked>
                <label for="site_theme_1"></label>
              </span>
              <span>
                <input type="radio" name="site_theme" value="dark" id="site_theme_2">
                <label for="site_theme_2"></label>
              </span>
              <span>
                <input type="radio" name="site_theme" value="translucent" id="site_theme_3">
                <label for="site_theme_3"></label>
              </span>
            </li>
            <li class="drop-title"><p>LOGO</p></li>
            <li class="drop-skin-li clearfix">
              <span class="inverse">
                <input type="radio" name="logo_bg" value="default" id="logo_bg_1" checked>
                <label for="logo_bg_1"></label>
              </span>
              <span>
                <input type="radio" name="logo_bg" value="color_2" id="logo_bg_2">
                <label for="logo_bg_2"></label>
              </span>
              <span>
                <input type="radio" name="logo_bg" value="color_3" id="logo_bg_3">
                <label for="logo_bg_3"></label>
              </span>
              <span>
                <input type="radio" name="logo_bg" value="color_4" id="logo_bg_4">
                <label for="logo_bg_4"></label>
              </span>
              <span>
                <input type="radio" name="logo_bg" value="color_5" id="logo_bg_5">
                <label for="logo_bg_5"></label>
              </span>
              <span>
                <input type="radio" name="logo_bg" value="color_6" id="logo_bg_6">
                <label for="logo_bg_6"></label>
              </span>
              <span>
                <input type="radio" name="logo_bg" value="color_7" id="logo_bg_7">
                <label for="logo_bg_7"></label>
              </span>
              <span>
                <input type="radio" name="logo_bg" value="color_8" id="logo_bg_8">
                <label for="logo_bg_8"></label>
              </span>
            </li>
            <li class="drop-title"><p>头部</p></li>
            <li class="drop-skin-li clearfix">
              <span class="inverse">
                <input type="radio" name="header_bg" value="default" id="header_bg_1" checked>
                <label for="header_bg_1"></label>                      
              </span>                                                    
              <span>                                                     
                <input type="radio" name="header_bg" value="color_2" id="header_bg_2">
                <label for="header_bg_2"></label>                      
              </span>                                                    
              <span>                                                     
                <input type="radio" name="header_bg" value="color_3" id="header_bg_3">
                <label for="header_bg_3"></label>
              </span>
              <span>
                <input type="radio" name="header_bg" value="color_4" id="header_bg_4">
                <label for="header_bg_4"></label>                      
              </span>                                                    
              <span>                                                     
                <input type="radio" name="header_bg" value="color_5" id="header_bg_5">
                <label for="header_bg_5"></label>                      
              </span>                                                    
              <span>                                                     
                <input type="radio" name="header_bg" value="color_6" id="header_bg_6">
                <label for="header_bg_6"></label>                      
              </span>                                                    
              <span>                                                     
                <input type="radio" name="header_bg" value="color_7" id="header_bg_7">
                <label for="header_bg_7"></label>
              </span>
              <span>
                <input type="radio" name="header_bg" value="color_8" id="header_bg_8">
                <label for="header_bg_8"></label>
              </span>
              </li>
            <li class="drop-title"><p>侧边栏</p></li>
            <li class="drop-skin-li clearfix">
              <span class="inverse">
                <input type="radio" name="sidebar_bg" value="default" id="sidebar_bg_1" checked>
                <label for="sidebar_bg_1"></label>
              </span>
              <span>
                <input type="radio" name="sidebar_bg" value="color_2" id="sidebar_bg_2">
                <label for="sidebar_bg_2"></label>
              </span>
              <span>
                <input type="radio" name="sidebar_bg" value="color_3" id="sidebar_bg_3">
                <label for="sidebar_bg_3"></label>
              </span>
              <span>
                <input type="radio" name="sidebar_bg" value="color_4" id="sidebar_bg_4">
                <label for="sidebar_bg_4"></label>
              </span>
              <span>
                <input type="radio" name="sidebar_bg" value="color_5" id="sidebar_bg_5">
                <label for="sidebar_bg_5"></label>
              </span>
              <span>
                <input type="radio" name="sidebar_bg" value="color_6" id="sidebar_bg_6">
                <label for="sidebar_bg_6"></label>
              </span>
              <span>
                <input type="radio" name="sidebar_bg" value="color_7" id="sidebar_bg_7">
                <label for="sidebar_bg_7"></label>
              </span>
              <span>
                <input type="radio" name="sidebar_bg" value="color_8" id="sidebar_bg_8">
                <label for="sidebar_bg_8"></label>
              </span>
            </li>
          </ul>
        </li>
        <!--切换主题配色-->
        <li class="dropdown dropdown-profile">
          <a href="javascript:void(0)" data-toggle="dropdown" class="dropdown-toggle">
            <!-- <img class="img-avatar img-avatar-48 m-r-10" src="images/users/avatar.jpg" alt="笔下光年" /> -->
            <span><?php echo $LoginAdmin['nickname']; ?></span>
          </a>
          <ul class="dropdown-menu dropdown-menu-right">
            <li>
              <a class="dropdown-item" href="lyear_pages_profile.html"><i class="mdi mdi-account"></i> 个人信息</a>
            </li>
            <li>
              <a class="dropdown-item" href="lyear_pages_edit_pwd.html"><i class="mdi mdi-lock-outline"></i> 修改密码</a>
            </li>
            <li>
              <a class="dropdown-item" href="javascript:void(0)"><i class="mdi mdi-delete"></i> 清空缓存</a>
            </li>
            <li class="dropdown-divider"></li>
            <li>
              <a class="dropdown-item" href="<?php echo url('admin/index/logout'); ?>"><i class="mdi mdi-logout-variant"></i> 退出登录</a>
            </li>
          </ul>
        </li>
      </ul>
      
    </nav>
    
  </header>
  <!--End 头部信息-->

            <!--帖子管理-->
<main class="lyear-layout-content">

    <div class="container-fluid p-t-15">
  
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <span class="layui-breadcrumb">
              <a><cite>帖子管理</cite></a>
            </span>
            <div class="card-toolbar d-flex flex-column flex-md-row">
              <div class="toolbar-btn-action">
                <!-- <a class="btn btn-success m-r-5 ajax-post confirm"  target-form="ids"><i class="mdi mdi-check"></i> 启用</a> -->
                <!-- <a class="btn btn-warning m-r-5" href="#!"><i class="mdi mdi-block-helper"></i> 禁用</a> -->
                <a class="btn btn-danger delAll"><i class="mdi mdi-window-close"></i> 批量删除</a>
              </div>
  
              <form class="search-bar" method="get" action="#!" role="form">
                <input type="hidden" name="search_field" id="search-field" value="title" />
                <div class="input-group ml-md-auto">
                  <div class="input-group-prepend">
                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false" id="search-btn">标题</button>
                    <div class="dropdown-menu" style="">
                      <a class="dropdown-item" href="#!" data-field="title">标题</a>
                      <a class="dropdown-item" href="#!" data-field="cat_name">栏目</a>
                    </div>
                  </div>
                  <input type="text" class="form-control" name="keyword" placeholder="请输入名称">
                </div>
              </form>
            </div>
            <div class="card-body">
  
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="check-all">
                          <label class="custom-control-label" for="check-all"></label>
                        </div>
                      </th>
                      <th>编号</th>
                      <th>标题</th>
                      <th>作者</th>
                      <th>内容</th>
                      <th>积分</th>
                      <th>所属分类</th>
                      <th>浏览量</th>
                      <th>创建时间</th>
                      <th>状态</th>
                      <th>操作</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($PostList as $key => $item): ?>
                    <tr>
                      <td>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input ids" name="ids[]" value="<?php echo $item['id']; ?>" id="<?php echo $item['id']; ?>">
                          <label class="custom-control-label" for="<?php echo $item['id']; ?>"></label>
                        </div>
                      </td>
                      <td><?php echo $key+1; ?></td>
                      <td><a href="<?php echo url('home/user/index', ['userid' => $item['userid']]); ?>"><?php echo $item['title']; ?></a></td>
                      <td><?php echo $item['user']['nickname']; ?></td>
                      <td><?php echo $item['content']; ?></td>
                      <td><?php echo $item['point']; ?></td>
                      <td><?php echo $item['cate']['name']; ?></td>
                      <td><?php echo $item['visit_count']; ?></td>
                      <td><?php echo $item['createtime']; ?></td>
                      <td>
                        <?php if($item['state']==0): ?>
                        <button class="layui-btn layui-btn-primary layui-btn-sm">正常</button>
                        <?php elseif($item['state']==1): ?>
                        <button class="layui-btn layui-btn-warm layui-btn-sm">置顶</button>
                        <?php elseif($item['state']==2): ?>
                        <button class="layui-btn layui-btn-danger layui-btn-sm">精华</button>
                        <?php elseif($item['state']==3): ?>
                        <button class="layui-btn layui-btn-normal layui-btn-sm">热门</button>
                        <?php endif; if($item['accept']>0): ?>
                        <button class="layui-btn layui-btn-warm layui-btn-sm">已采纳</button>
                        <?php else: ?>
                        <button class="layui-btn layui-btn-success layui-btn-sm">未采纳</button>
                        <?php endif; ?>
                      </td>
                      <td>
                        <div class="btn-group">
                          <a class="btn btn-xs btn-default" href="#!" title="" data-toggle="tooltip" data-original-title="帖子详情"><i class="mdi mdi-pencil"></i></a>
                          <a class="btn btn-xs btn-default del" title="" data-toggle="tooltip" data-id="<?php echo $item['id']; ?>" data-original-title="删除"><i class="mdi mdi-window-close"></i></a>
                        </div>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
              
              <!-- 分页 -->
              <td>
				<?php echo $PostList->render(); ?>
              </td>
  
            </div>
          </div>
        </div>
  
      </div>
  
    </div>
  
  </main>
  <script src="/static/home/res/layui/layui.js"></script>
  <script>
    layui.use(['layer'], function () {
      var $ = layui.jquery,
        layer = layui.layer
  
        // 删除
      $('.del').click(function () {
        let id = $(this).data('id');
        console.log(id)
        layer.confirm('确定删除该帖子？', { icon: 3, title: '提示' }, function (index) {
          // 发起请求
          $.ajax({
            type: 'post',
            url: `<?php echo url('admin/Post/del'); ?>`,
            data: {
              id
            },
            dataType: 'json',
            success: function (res) {
              if (res.code == 1) {
                layer.msg(res.msg, { icon: 1, time: 1500 }, function (index) {
                  location.href = res.url
                  // location.reload()
                  layer.close(index)
                })
              } else {
                layer.msg(res.msg, { icon: 2, time: 1500 }, function (index) {
                  layer.close(index)
                  location.reload()
                })
              }
            }
          })
          layer.close(index)
        })
      })
  
      function GetId () {
        // 存放id的数组
        let list = []
        $('input[name="ids[]"]:checked').each(function () {
          // console.log(item);
          list.push($(this).val())
        })
        return list
      }
  
      // 批量删除
      $('.delAll').click(function () {
        layer.confirm('确定批量删除？', { icon: 3, title: '提示' }, function (index) {
          let list = GetId()
          if (list.length == 0) {
            // alert("至少选择一条帖子!")
            layer.alert('至少选择一条帖子!')
            return false
          }
          // console.log(list)
          $.ajax({
            type: 'post',
            url: `<?php echo url('admin/Post/delAll'); ?>`,
            data: {
              list,
              action: 'delAll'
            },
            dataType: 'json',
            success: function (res) {
              if (res.code == 1) {
                layer.alert(res.msg, function (index) {
                  // location.reload()
                  location.href = res.url
                  layer.close(index);
                });
              } else {
                layer.alert(res.msg, function (index) {
                  location.reload()
                  layer.close(index);
                });
              }
            }
          })
          layer.close(index)
        })
      })
  
      // 批量启用
      $('#stateup').click(function () {
        let res = confirm('确认批量启用？')
        if (!res) {
          return false
        }
        let list = GetId()
        if (list.length == 0) {
          alert("至少选择一名管理员!")
          return false
        }
        console.log(list)
        $.ajax({
          type: 'post',
          url: `<?php echo url('admin/admin/stateUp'); ?>`,
          data: {
            list,
            action: 'stateup'
          },
          dataType: 'json',
          success: function (res) {
            if (res.code == 1) {
              layer.msg(res.msg, { icon: 1, time: 1500 }, function (index) {
                // location.href = res.url
                layer.close(index)
                location.reload()
              })
            } else {
              layer.msg(res.msg, { icon: 1, time: 1500 }, function (index) {
                // location.href = res.url
                layer.close(index)
                location.reload()
              })
            }
          },
        })
      })
  
      // 批量禁用
      $('#statedown').click(function () {
        let res = confirm('确认批量禁用？')
        if (!res) {
          return false
        }
        let list = GetId()
        if (list.length == 0) {
          alert("至少选择一名管理员!")
          return false
        }
        console.log(list)
        $.ajax({
          type: 'post',
          url: `<?php echo url('admin/admin/stateDown'); ?>`,
          data: {
            list,
            action: 'stateDown'
          },
          dataType: 'json',
          success: function (res) {
            if (res.code == 1) {
              layer.msg(res.msg, { icon: 1, time: 1500 }, function (index) {
                // location.href = res.url
                layer.close(index)
                location.reload()
              })
            } else {
              layer.msg(res.msg, { icon: 1, time: 1500 }, function (index) {
                // location.href = res.url
                layer.close(index)
                location.reload()
              })
            }
          },
        })
      })
    })
  
  </script>
        </div>
    </div>
</body>
</html>