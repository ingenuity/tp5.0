<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:49:"F:\!Abeifen\WWW\tp5.0\thinkphp\tpl\admin_jump.tpl";i:1653962306;}*/ ?>

<!DOCTYPE html>
<html lang="zh">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
  <title>ASK提示跳转</title>
  <link href="/static/admin/css/bootstrap.min.css" rel="stylesheet">
  <link href="/static/admin/css/materialdesignicons.min.css" rel="stylesheet">
  <link href="/static/admin/css/style.min.css" rel="stylesheet">
  <style>
    body{
        background-color: #fff;
    }
    .error-page {
        height: 100%;
        position: fixed;
        width: 100%;
    }
    .error-body {
        padding-top: 5%;
    }
    .error-body h1 {
        font-size: 210px;
        font-weight: 700;
        text-shadow: 4px 4px 0 #f5f6fa, 6px 6px 0 #33cabb;
        line-height: 210px;
        color: #33cabb;
    }
    .error-body h4 {
        margin: 30px 0px;
    }

    .notice{
      width:200px;
      height:200px;
      margin:0 auto;
    }

    .notice img{
      width:100%;
    }
  </style>
</head>
  
<body>
  <section class="error-page">
    <div class="error-box">
      <div class="error-body text-center">
        <?php if($code == 1){?>
          <div class="notice">
            <img src="/static/home/res/images/success.svg" />
          </div>
        <?php }else{ ?>
          <div class="notice">
            <img src="/static/home/res/images/error.svg" />
          </div>
        <?php }?>
        <h4><?php echo strip_tags($msg); ?></h4>
        等待<b id="time"><?php echo $wait;?></b>秒后自动跳转，或者
          <a id="url" href="<?php echo $url;?>">点击链接跳转</a>
      </div>
    </div>
  </section>
</body>
</html>
<script type="text/javascript" src="/static/admin/js/jquery.min.js"></script>
<script type="text/javascript" src="/static/admin/js/bootstrap.min.js"></script>
<script>

  //获取链接地址
  var url = document.getElementById('url').getAttribute('href')

  function autoplay()
  {
    var time = document.getElementById('time')
    var sec = parseInt(time.innerText)

    if(sec <= 0)
    {
      clearInterval(T)

      //要跳转
      location.href = url

      return;
    }

    time.innerText = sec-1;
  }

  var T = setInterval(autoplay, 1000)
  
</script>