<?php
namespace app\admin\controller;

use app\common\controller\Backend;

class Index extends Backend
{
    public $NoNeedLogin = ['login'];
    
    public function index()
    {
        return $this->fetch();
    }

    //后台登录
    public function login()
    {
        $LoginAdmin = !empty(cookie('LoginAdmin')) ? cookie('LoginAdmin') : '';

        if($LoginAdmin)
        {
            $adminList = model('Admin')->select();

            $adminid = 0;

            foreach($adminList as $item)
            {
                $val = md5($item['id'].$item['salt']);

                if($LoginAdmin == $val)
                {
                    $adminid = $item['id'];
                    break;
                }
            }

            $admin = model('Admin')->find($adminid);

            if($admin)
            {
                cookie('LoginAdmin',null);

                $this->error('非法登录',url('admin/index/login'));
            }

            // 判断要登录的账号的状态是否被禁
            if($admin['state'] != 1)
            {
                cookie('LoginAdmin',null);
                $this->error('该管理员已禁用');
            }

            $this->success('您已经登录了，无须重复登录',url('admin/index/index'));
        }

        // 判断是否有数据提交
        if($this->request->isPost())
        {
            $params = $this->request->param();

            // 判断验证码
            // if(!captcha_check($params['captcha']))
            // {
            //     $this->error('验证码错误');
            // }

            // 判断用户
            $admin = model('Admin')->where('username',$params['username'])->find();

            if(!$admin)
            {
                $this->error('该管理员不存在');
            }

            // 判断密码
            $password = md5($params['password'].$admin['salt']);

            if($password != $admin['password'])
            {
                $this->error('密码错误');
            }

            // 判断管理员状态
            if($admin['state'] != 1)
            {
                cookie('LoginAdmin',null);
                $this->error('管理员已禁用');
            }

            // 判断是否保留五天记录自动登录
            if(!empty($params['rememberme']) && $params['rememberme'] == 1)
            {
                cookie('LoginAdmin',md5($admin['id'].$admin['salt']),5 * 24 * 3600 + time());
            }else{
                cookie('LoginAdmin',md5($admin['id'].$admin['salt']));
            }

            $this->success('登录成功',url('admin/index/index'));
        }
    

        //动态使用模板布局
        $this->view->engine->layout(false);
        return $this->fetch();
    }

    //退出
    public function logout()
    {
        cookie('LoginUser',null);
        $this->success('退出成功',url('admin/index/login'));
    }
}
