<?php

namespace app\common\model\product;

use think\Model;

class Order extends Model
{
    // 订单表
    protected $table = 'pre_order';

    // 自动写入时间
    protected $autoWriteTimestamp = true;

    // 创建时间 => 注册时间
    protected $createTime = 'createtime';

    // 更新时间
    protected $updateTime = false;

    protected $append = [
        'status_text', //多张
    ];

    protected function getStatusTextAttr($value, $data)
    {
        $status = $data['status'];
        $text = '';

        switch($status)
        {
            case 1:
                $text = '已支付';
                break;
            case 2:
                $text = '已发货';
                break;
            case 3:
                $text = '已收货';
                break;
            case 4:
                $text = '已评价';
                break;
            case -1:
                $text = '已退货';
                break;
            case -2:
                $text = '退货中';
                break;
            case -3:
                $text = '拒绝退货';
                break;
            default:
                $text= '未知状态';
        }

        return $text;
    }
}
