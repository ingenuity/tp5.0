<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:48:"F:\!Abeifen\WWW\tp5.0\thinkphp\tpl\home_jump.tpl";i:1653096286;}*/ ?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>ASK提示跳转</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link rel="stylesheet" href="/static/home/res/layui/css/layui.css">
  <link rel="stylesheet" href="/static/home/res/css/global.css">
  <style>
    .notice{
      width:200px;
      height:200px;
      margin:0 auto;
    }

    .notice img{
      width:100%;
    }
  </style>
</head>
<body>
  <div class="fly-header layui-bg-black">
    <div class="layui-container">
      <a class="fly-logo" href="<?php echo url('home/index/index'); ?>">
        <img src="/static/home/res/images/logo.png" alt="layui">
      </a>
    </div>
  </div>

  <div class="layui-container fly-marginTop">
    <div class="fly-panel">
      <div class="fly-none">
        <?php if($code == 1){?>
          <div class="notice">
            <img src="/static/home/res/images/success.svg" />
          </div>
        <?php }else{ ?>
          <div class="notice">
            <img src="/static/home/res/images/error.svg" />
          </div>
        <?php }?>

        <p>
          <?php echo strip_tags($msg); ?><br />
          等待<b id="time"><?php echo $wait;?></b>秒后自动跳转，或者
          <a id="url" href="<?php echo $url;?>">点击链接跳转</a>
        </p>
      </div>
    </div>
  </div>

  <div class="fly-footer">
    <p>2017 &copy; <a href="http://www.layui.com/" target="_blank">Fly技术社区</a></p>
  </div>
</body>
</html>
<script>

  //获取链接地址
  var url = document.getElementById('url').getAttribute('href')

  function autoplay()
  {
    var time = document.getElementById('time')
    var sec = parseInt(time.innerText)

    if(sec <= 0)
    {
      clearInterval(T)

      //要跳转
      location.href = url

      return;
    }

    time.innerText = sec-1;
  }

  var T = setInterval(autoplay, 1000)
  
</script>