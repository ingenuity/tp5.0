<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Controller;
use think\Request;

class Analysis extends Backend
{
   public function user()
   {
        if($this->request->isAjax())
        {
            $action = $this->request->param('action');

            if($action == 'pie')
            {
                $sex0 =model('User.User')->where('sex',0)->count();
                $sex1 =model('User.User')->where('sex',1)->count();
                $sex2 =model('User.User')->where('sex',2)->count();

                $auth0 =model('User.User')->where('auth',0)->count();
                $auth1 =model('User.User')->where('auth',1)->count();

                $data = [
                    'sex' => [$sex0,$sex1,$sex2],
                    'auth' => [$auth0,$auth1]
                ];

                $this->success('用户统计',null,$data);
            }
        }


       return $this->fetch();
   }
}
