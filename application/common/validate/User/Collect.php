<?php

namespace app\common\validate\User;

use think\Validate;

// 收藏验证器
class Collect extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        
        'userid' => 'require', //必填
        'postid' => 'require', //必填
        
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'userid.require'  => '未知用户',
        'postid.require' => '未知帖子',
    ];
}