<?php

namespace app\common\model\Product;

use think\Model;

class Type extends Model
{
    // 商品分类表
    protected $table = 'pre_product_type';

    // 追加字段
    protected $append = [
        'cover_cdn',
    ];

    // 封面图
    public function getCoverCdnAttr($value,$data)
    {
        // halt(@is_file('.'.$data['cover']));
        $cover = @is_file('.'.$data['cover']) ? $data['cover'] : '/static/home/res/images/back_1.jpg';

        $cdn = model('Config')->where(['key' => 'SiteUrl'])->value('value');

        // $cdn = config('cdn');

        return $cdn.$cover;
    }
}
