<?php

namespace app\common\model\Post;

use think\Model;

//引用软删除
use traits\model\SoftDelete;

class Comment extends Model
{
    //评论表
    protected $table = 'pre_comment';

     // 自动写入时间
     protected $autoWriteTimestamp = true;

     // 创建时间 => 注册时间
     protected $createTime = 'createtime';
 
     // 更新时间
     protected $updateTime = false;
 
     //软删除
     use SoftDelete;
     protected $deleteTime = 'deletetime';
 
     protected $dateFormat = 'Y-m-d';

    //关联帖子模型
    public function post()
    {
        return $this->belongsTo('app\common\model\Post\Post','postid','id',[],'LEFT')->setEagerlyType(0);
    }

    //关联用户模型
    public function user()
    {
        return $this->belongsTo('app\common\model\User\User','userid','id',[],'LEFT')->setEagerlyType(0);
    }
}
