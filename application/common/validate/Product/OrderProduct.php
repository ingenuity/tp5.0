<?php

namespace app\common\validate\Product;

//引入底层的验证器类
use think\Validate;

/**
 * 订单商品验证器
 */
class OrderProduct extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'orderid' => 'require', //必填
        'proid' => 'require', //必填
        'nums' => 'require|gt:0', //必填
        'price' => 'require|egt:0', //必填
        'total' => 'require|egt:0', //必填
        'userid' => 'require'
    ];

    /**
     * 提示消息
     */
    protected $message = [
        'orderid.require'    => '订单ID未知',
        'proid.require'    => '商品ID未知',
        'nums.require'    => '请填写商品数量',
        'price.require'    => '请填写商品的单价',
        'total.require'    => '请填写商品的总价',
        'userid.require'    => '用户ID未知',
    ];
    
    /**
     * 验证场景
     */
    protected $scene = [
    ];
}
