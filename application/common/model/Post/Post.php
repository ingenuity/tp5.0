<?php

namespace app\common\model\Post;

use think\Model;

//引用软删除
use traits\model\SoftDelete;

class Post extends Model
{
    // 帖子表
    protected $table = 'pre_post';

    // 自动写入时间
    protected $autoWriteTimestamp = true;

    // 创建时间 => 注册时间
    protected $createTime = 'createtime';

    // 更新时间
    protected $updateTime = false;

    //软删除
    use SoftDelete;
    protected $deleteTime = 'deletetime';

    protected $dateFormat = 'Y-m-d';

    // 关联查询 => 分类
    public function cate()
    {
        return $this->belongsTo('app\common\model\Post\Cate','cateid','id',[],'LEFT')->setEagerlyType(0);
    }

    // 关联查询 => 用户
    public function user()
    {
        return $this->belongsTo('app\common\model\User\User','userid','id',[],'LEFT')->setEagerlyType(0);
    }

    // 评论总数字段
    public function getCommentCountAttr($value,$data)
    {
        // 怎么去查询评论表数据 如果没条件就全部 首先有一个条件 在评论表有外键 => postid
        $count = model('Post.Comment')->where(['postid' => $data['id']])->count();

        return $count;
    }

    //获取帖子状态的积分
    public function state()
    {
        //$PostState = model('Config')->where('key','like',"%PostState%")->select();

        $PostState1 = model('Config')->where('key','PostState1')->value('value');
        $PostState2 = model('Config')->where('key','PostState2')->value('value');
        $PostState3 = model('Config')->where('key','PostState3')->value('value');

        $StateList = [1 => "置顶 {$PostState1}积分",2 => "精华 {$PostState2}积分",1 => "热门 {$PostState3}积分"];

        return $StateList;
    }
    // 获取浏览量
    public function getVisitCountAttr($value,$data)
    {
        $visitArr = explode(',',$data['visit']);
        
        $visitArr = array_filter($visitArr);

        $VisitCount = count($visitArr);

        return $VisitCount;
    }
}
